<?php
get_header();
while(have_posts()): the_post();
?>

    <div class="container-fluid wrap">
        <div class="row center-xs">
            <div class="col-xs-12 col-sm-10 col-md-10 col-lg-9 margin-top__medium start-xs">

                <div class="margin-top__big margin-bottom__mega--x">
                    <a class="font-weight__normal text-color__text btn__size--normal border-radius__normal background-color__grey" href="<?php the_permalink(1308); ?>"><i class="fas fa-long-arrow-alt-left"></i> Webinars</a>
                </div>

                <article class="article-single border-color__grey--regent card background-color__white border-radius__medium card__size--mega box-shadow__small">

                    <div class="article-metas center-xs">
                        WEBINAR
                    </div>

                    <h1 class="article-title center-xs font-size__mega"><?php the_title(); ?></h1>

                    <?php if(get_field('webinar_video') || get_field('webinar_video_html')): ?>
                        <div class="article-video border-radius__normal">
                            <?php if(get_field('webinar_video')): ?>
                                <?php the_field('webinar_video'); ?>
                            <?php elseif(get_field('webinar_video_html')): ?>
                                <?php the_field('webinar_video_html'); ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <div class="article-content">
                        <?php the_content(); ?>
                    </div>

                    <div class="article-footer margin-top__mega padding-top__mega">
                        <div class="row center-xs">
                            <div class="col-xs-10 col-sm-4 start-xs">
                                <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                    <article class="webinar__author margin-bottom__normal">
                                        <div class="row middle-xs">
                                            <div class="col-xs-4">
                                                <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                            </div>
                                            <div class="col-xs-8">
                                                 <span class="name font-size__small--x">Por <strong><?php the_sub_field('name'); ?></strong></span>
                                                 <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                            </div>
                                        </div>
                                    </article>
                                <?php endwhile; endif; ?>
                            </div>
                            <div class="col-xs-10 col-sm-8 end-xs meta-column">
                                <time class="meta font-size__small--x font-weight__normal text-color__text"><i class="far fa-calendar text-color__main" aria-hidden="true"></i>
                                <time datetime="1591292775453"><?php echo get_the_date(); ?></time></time>

                                <div class="article-share margin-top__normal">
                                    <a href="https://www.facebook.com/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-right__normal  font-size__small border-radius__rounded text-color__white btn__size--normal background-color__main">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>

                                    <a href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-right__normal  font-size__small border-radius__rounded text-color__white btn__size--normal background-color__main">
                                        <i class="fab fa-twitter"></i>
                                    </a>

                                    <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_the_permalink(); ?>" target="_blank" class="btn font-size__small border-radius__rounded text-color__white btn__size--normal background-color__main">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

            <div class="blog-posts blog-posts--related webinar-list margin-top__mega--x">
                <div class="col-xs-12 margin-top__mega--x start-xs">
                    <h4 class="margin-bottom__mega">Otros Webinars</h4>
                    <div class="row">
                       <?php
                        $current_webinar = get_the_ID();
                        $args = (array(
                           'post_type' => 'guru_webinar',
                           'posts_per_page' => 3,
                           'post__not_in' => array($current_webinar)
                        ) );
                        $query = new WP_Query($args);
                        if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
                       ?>
                        <div class="col-md-4">
                            <div class="webinar-item card background-color__white border-radius__normal box-shadow__medium margin-bottom__big" data-aos="fade-up">

                                <div class="webinar-item__thumb">
                                    <a href="<?php the_permalink( ); ?>">
                                        <?php the_post_thumbnail('medium_large'); ?>
                                    </a>
                                </div>

                                <div class="webinar-item__content padding__mega">
                                    <div class="row margin-bottom__medium">
                                        <div class="col-xs-6">
                                            <time class="meta font-size__small--x font-weight__normal text-color__text"><i class="far fa-calendar text-color__main" aria-hidden="true"></i>
                                            <time datetime="1591292775453"><?php echo get_the_date(); ?></time></time>
                                        </div>
                                        <div class="col-xs-6 end-xs">
                                            <?php if(get_field('webinar_duration')): ?>
                                                <i class="fa fa-clock text-color__main"></i>
                                                <?php the_field('webinar_duration'); ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <a href="<?php the_permalink(); ?>"><h2 class="article-title font-size__medium margin-bottom__normal"><?php the_title(); ?></h2></a>

                                    <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                        <article class="webinar__author margin-bottom__normal">
                                            <div class="row middle-xs">
                                                <div class="col-xs-3 col-sm-4">
                                                    <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                                </div>
                                                <div class="col-xs-8">
                                                     <span class="name font-size__small--x">Por <strong><?php the_sub_field('name'); ?></strong></span>
                                                     <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                                </div>
                                            </div>
                                        </article>
                                    <?php endwhile; endif; ?>

                                    <a href="<?php the_permalink(); ?>" class="btn__read font-weight__normal font-size__small--x text-color__main"><?php _e('Ver webinar', 'apollo'); ?><i class="fas fa-long-arrow-alt-right margin-left__normal" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                            </article>
                        </div>
                   <?php endwhile; endif; wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php endwhile; get_footer();
