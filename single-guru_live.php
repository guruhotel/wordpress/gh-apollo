<?php
get_header();
while(have_posts()): the_post();
    $terms = wp_get_post_terms( get_the_ID(), 'guru_live_type');
?>
    <div class="page-live <?php echo $terms[0]->slug; ?>">
        <div class="background-color__white padding__section section-intro">
            <div class="container-fluid wrap">
                <div class="row middle-xs center-xs">
                    <div class="col-xs-11 col-md-4 margin-bottom__mega--x start-xs">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/guruhotel-live.svg">
                        <h1 class="font-size__big text-color__titles"><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                    </div>
                    <div class="col-xs-11 col-md-7 col-md-offset-1 start-xs">
                        <div class="page-content border-color__grey--regent card background-color__white border-radius__medium card__size--mega box-shadow__small"><?php the_field('live_iframe'); ?></div>
                    </div>
                </div>
            </div>
        </div>

        <?php
           $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
           $post_id = get_the_ID();
           $args = (array(
               'post_type' => 'guru_live',
               'posts_per_page' => 3,
               'paged' => $paged,
               'post__not_in' => array($post_id)
           ) );
           $query = new WP_Query($args);
           if($query->have_posts()) :;
       ?>
        <div class="webinar-list padding__small-section webinar-list__recent">
            <div class="webinar-list__webinars active" id="recent-webinars">
                <div class="container-fluid wrap center-xs">
                    <h2 class="margin-bottom__mega--x"><?php _e('Recent Lives', 'gh-apollo'); ?></h2>
                    <div class="row start-xs">
                           <?php
                               $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                               $post_id = get_the_ID();
                               $args = (array(
                                   'post_type' => 'guru_live',
                                   'posts_per_page' => 9,
                                   'paged' => $paged,
                                   'post__not_in' => array($post_id)
                               ) );
                               $query = new WP_Query($args);
                               while($query->have_posts()) : $query->the_post();
                           ?>
                            <div class="col-sm-6 col-md-4">
                                <div class="webinar-item card background-color__white border-radius__normal box-shadow__medium margin-bottom__big" data-aos="fade-up">

                                    <div class="webinar-item__thumb">
                                        <a href="<?php the_permalink( ); ?>">
                                            <?php the_post_thumbnail('medium_large'); ?>
                                        </a>
                                    </div>

                                    <div class="webinar-item__content padding__mega">
                                        <div class="row">
                                            <div class="col-xs-5 font-size__small--x ">
                                                <h4 class="banner-preline font-size__small--x text-transform__uppercase text-color__main"><i class="text-color__main margin-right__normal <?php if($terms[0]->slug == 'live') echo 'fas fa-play'; else echo 'fas fa-microphone-alt'; ?>"></i><?php echo $terms[0]->name; ?></h4>
                                            </div>

                                            <div class="col-xs-7 end-xs ">
                                                    <time class="meta font-size__small--x font-weight__normal text-color__text margin-bottom__medium"><i class="far fa-calendar text-color__main" aria-hidden="true"></i>
                                                <time><?php echo get_the_date(); ?></time></time>
                                            </div>
                                        </div>

                                        <a href="<?php the_permalink(); ?>"><h2 class="article-title font-size__medium margin-bottom__normal"><?php the_title(); ?></h2></a>

                                        <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                            <article class="webinar__author margin-bottom__normal">
                                                <div class="row middle-xs">
                                                    <div class="col-xs-4">
                                                        <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                                    </div>
                                                    <div class="col-xs-8">
                                                         <span class="name font-size__small--x">Por <strong><?php the_sub_field('name'); ?></strong></span>
                                                         <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                                    </div>
                                                </div>
                                            </article>
                                        <?php endwhile; endif; ?>

                                        <a href="<?php the_permalink(); ?>" class="btn__read font-weight__normal font-size__small--x text-color__main"><?php _e('Watch Live', 'apollo'); ?><i class="fas fa-long-arrow-alt-right margin-left__normal" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>

                                </article>
                            </div>
                           <?php endwhile; ?>

                            <div class="pagination">
                            <?php
                              $big = 999999999; // need an unlikely integer

                              echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total' => $query->max_num_pages,
                                'prev_text' => '<',
                                'next_text' => '>'
                              ) );
                            ?>
                            </div>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <section class="pre-footer padding-top__mega--x padding-bottom__mega--x background-color__main text-color__white">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/footer-illustration.svg" class="pre-footer-img">
            <div class="container-fluid wrap">
                <div class="row middle-xs">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-offset-1">
                        <h2 class="font-size__mega text-color__white"><?php the_field('footer_cta_title', 1979); ?></h2>
                        <p><?php the_field('footer_cta_text', 1979); ?></p>
                        <a href="<?php the_field('footer_cta_url', 1979); ?>" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__normal font-size__small--x"><?php the_field('footer_cta_label', 1979); ?></a>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php endwhile; get_footer();
