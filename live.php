<?php
/**
 * Template Name: Live
 */
get_header();
while(have_posts()): the_post();
?>

        <div class="background-color__main padding__section text-color__white section-intro">
            <?php
                $first = true;
                if(have_rows('live_slider')) : while(have_rows('live_slider')): the_row();
            ?>
                <div>
                    <div class="container-fluid wrap">
                        <div class="row middle-xs">

                            <div class="col-sm-4 col-md-5 image-column last-md">
                                <?php if(get_sub_field('iframe')): ?>
                                    <?php the_sub_field('iframe'); ?>
                                <?php else: ?>
                                    <a href="<?php the_sub_field('link'); ?>" class="webinar-thumb">
                                        <img src="<?php $img = get_sub_field('img'); echo $img['sizes']['medium_large']; ?>" class="border-radius__normal box-shadow__normal">
                                    </a>
                                <?php endif; ?>

                                <div class="col-md-2 author-column first-md hidden__lg hidden__md hidden__xs">
                                    <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                        <article class="webinar__author margin-bottom__medium">
                                            <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                            <div>
                                                <span class="name font-size__small--x"><?php _e('By', 'gh-apollo'); ?> <strong><?php the_sub_field('name'); ?></strong></span>
                                                <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                            </div>
                                        </article>
                                    <?php endwhile; endif; ?>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-8 col-md-5 content-column">
                                <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white" data-aos="fade-up" data-aos-delay="200"><?php the_sub_field('pretitle'); ?></h4>

                                <h1 class="font-size__mega text-color__white"><?php the_sub_field('title'); ?></h1>

                                <div class="upcoming-webinar__data">
                                    <i class="fas fa-calendar-alt"></i>
                                    <span>
                                        <?php
                                            $date_string = get_sub_field('date');
                                            $date = DateTime::createFromFormat('d/m/Y g:i a', $date_string);
                                            echo $date->format('F j, Y \| g:i a');
                                        ?>
                                    </span>
                                    <span class="margin-left__normal margin-right__normal info__separator">|</span>
                                    <i class="fa fa-clock"></i>
                                    <span><?php the_sub_field('duration'); ?></span>
                                </div>

                                <div><?php the_sub_field('text'); ?></div>

                                <footer>
                                    <?php if(get_sub_field('live_url')): ?>
                                        <a href="<?php the_sub_field('live_url'); ?>" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__mega--x font-size__small--x margin-right__normal" data-lity><?php the_sub_field('btn_label'); ?><?php _e('Watch', 'gh-apollo'); ?><i class="fas fa-long-arrow-alt-right margin-left__normal" aria-hidden="true"></i></a>
                                        <a href="<?php the_sub_field('subscribe_url'); ?>" target="_blank" class="btn btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><i class="fab fa-youtube margin-right__normal"></i><?php _e('Subscribe', 'gh-apollo'); ?></a>
                                    <?php elseif(get_sub_field('subscribe_url')): ?>
                                        <a href="<?php the_sub_field('subscribe_url'); ?>" target="_blank" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><i class="fab fa-youtube margin-right__normal"></i><?php _e('Subscribe', 'gh-apollo'); ?></a>
                                    <?php elseif(get_sub_field('link')): ?>
                                        <a href="<?php the_sub_field('link'); ?>" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_sub_field('btn_label'); ?><i class="fas fa-long-arrow-alt-right margin-left__normal" aria-hidden="true"></i></a>
                                     <?php endif; ?>
                                </footer>
                            </div>

                            <div class="col-md-2 author-column first-md hidden__sm">
                                <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                    <article class="webinar__author margin-bottom__medium center-xs">
                                        <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                        <div>
                                            <span class="name font-size__small--x"><?php _e('By', 'gh-apollo'); ?> <strong><?php the_sub_field('name'); ?></strong></span>
                                            <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                        </div>
                                    </article>
                            <?php endwhile; endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>

    <div class="page-live">
       <div class="webinar-list padding__small-section webinar-list__recent">
        <div class="webinar-list__webinars active" id="recent-webinars">
            <div class="container-fluid wrap center-xs">
                <div class="container-fluid wrap end-xs">
                    <nav class="webinars__order-by live__order-by">
                        <ul>
                            <li class="all current">
                                <a href=".live-item" class="live__order-by-btn"><?php _e('All', 'gh-apollo'); ?></a>
                            </li>
                            <li>
                                <a href=".type-live" class="live__order-by-btn"><?php _e('Lives', 'gh-apollo'); ?></a>
                            </li>
                            <li>
                                <a href=".type-podcast" class="live__order-by-btn"><?php _e('Podcasts', 'gh-apollo'); ?></a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="row start-xs">
                       <?php
                           $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                           $args = (array(
                               'post_type' => 'guru_live',
                               'posts_per_page' => 9,
                               'paged' => $paged,
                           ) );
                           $query = new WP_Query($args);
                           if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
                                $terms = wp_get_post_terms( get_the_ID(), 'guru_live_type');
                       ?>
                        <div class="col-sm-6 col-md-4 live-item active type-<?php echo $terms[0]->slug; ?>">
                            <div class="webinar-item card background-color__white border-radius__normal box-shadow__medium margin-bottom__big " data-aos="fade-up">
                                <div class="webinar-item__thumb">
                                    <a href="<?php the_permalink( ); ?>">
                                        <?php the_post_thumbnail('medium_large'); ?>
                                    </a>
                                </div>

                                <div class="webinar-item__content padding__mega">
                                    <div class="row">
                                            <div class="col-xs-5 font-size__small--x ">
                                                <h4 class="banner-preline font-size__small--x text-transform__uppercase text-color__main"><i class="text-color__main margin-right__normal <?php if($terms[0]->slug == 'live') echo 'fas fa-play'; else echo 'fas fa-microphone-alt'; ?>"></i><?php echo $terms[0]->name; ?></h4>
                                            </div>

                                            <div class="col-xs-7 end-xs ">
                                                <time class="meta font-size__small--x font-weight__normal text-color__text margin-bottom__medium"><i class="far fa-calendar text-color__main" aria-hidden="true"></i>
                                            <time><?php echo get_the_date(); ?></time></time>
                                        </div>
                                    </div>

                                    <a href="<?php the_permalink(); ?>"><h2 class="article-title font-size__medium margin-bottom__normal"><?php the_title(); ?></h2></a>

                                    <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                        <article class="webinar__author margin-bottom__normal">
                                            <div class="row middle-xs">
                                                <div class="col-xs-4">
                                                    <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                                </div>
                                                <div class="col-xs-8">
                                                     <span class="name font-size__small--x">Por <strong><?php the_sub_field('name'); ?></strong></span>
                                                     <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                                </div>
                                            </div>
                                        </article>
                                    <?php endwhile; endif; ?>

                                    <a href="<?php the_permalink(); ?>" class="btn__read font-weight__normal font-size__small--x text-color__main"><?php if($terms[0]->slug == 'live') _e('Watch', 'gh-apollo'); else _e('Play', 'gh-apollo'); ?><i class="fas fa-long-arrow-alt-right margin-left__normal" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                            </article>
                        </div>
                       <?php endwhile; endif; ?>

                        <div class="pagination">
                            <?php
                              $big = 999999999; // need an unlikely integer

                              echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total' => $query->max_num_pages,
                                'prev_text' => '<',
                                'next_text' => '>'
                              ) );
                            ?>
                        </div>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>

        <section class="pre-footer padding-top__mega--x padding-bottom__mega--x background-color__main text-color__white">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/footer-illustration.svg" class="pre-footer-img">
            <div class="container-fluid wrap">
                <div class="row middle-xs">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-offset-1">
                        <h2 class="font-size__mega text-color__white"><?php the_field('footer_cta_title'); ?></h2>
                        <p><?php the_field('footer_cta_text'); ?></p>
                        <a href="<?php the_field('footer_cta_url'); ?>" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__normal font-size__small--x"><?php the_field('footer_cta_label'); ?></a>
                    </div>
                </div>
            </div>
        </section>
    </div>


<?php endwhile; get_footer();
