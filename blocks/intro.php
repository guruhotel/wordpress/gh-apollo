<?php
/**
 * Intro Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'intro-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'intro';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
?>
<section class="guru-block block__intro padding__section <?php if(get_field('bottom_deco')) echo 'guru-block--deco '; the_field('color_combination'); echo esc_attr(' '.$className); ?>" id="<?php echo esc_attr($id); ?>" >
        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-sm-12">
                    <?php if(get_field('intro_subline')): ?>
                        <h4 class="font-size__small--x pretitle without-margin__bottom"><?php the_field('intro_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('intro_title')): ?>
                        <h1 class="font-size__mega--x text-color__white">
                            <?php the_field('intro_title'); ?>
                            <?php if(get_field('intro_title_second_part')): ?>
                                <span><?php the_field('intro_title_second_part'); ?></span>
                            <?php endif; ?>
                        </h1>
                    <?php endif; ?>

                    <?php if(get_field('intro_text')): ?>
                        <p><?php the_field('intro_text'); ?></p>
                    <?php endif; ?>

                    <footer>
                        <a href="<?php the_field('intro_cta_link'); ?>" class="btn btn--primary border-radius__mega--x background-color__white text-transform__uppercase letter-spacing__medium font-weight__medium text-color__titles padding__medium--x display__inline--block margin-top__mega banner-button">
                            <?php the_field('intro_cta_text'); ?>
                        </a>

                        <?php if(get_field('intro_cta_secondary_text')): ?>
                            <a href="<?php the_field('intro_cta_secondary_link'); ?>" class="text-color__white btn--text margin-left__mega--x display__inline--block" data-lity><?php if(get_field('intro_cta_secondary_icon')): ?>
                            <i class="<?php the_field('intro_cta_secondary_icon'); ?> margin-right__normal"></i><?php endif; ?><?php the_field('intro_cta_secondary_text'); ?></a>
                        <?php endif; ?>
                    </footer>
                </div>

                <?php if(get_field('intro_img')): ?>
                    <div class="col-xs-11 col-sm-4 col-md-4 col-sm-offset-1 col-md-offset-0 block__intro__img">
                        <img src="<?php $img = get_field('intro_img'); echo $img['sizes']['medium_large']; ?>">
                    </div>
                <?php endif; ?>
            </div>
        </div>
</section>

<?php if (is_admin()) : ?>

<div class="alert alert-warning">
    La visualización estará disponible cuando se hayan completado los campos en la columna derecha.
</div>

<?php endif; ?>
