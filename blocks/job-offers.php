<?php
/**
 * Job Offers Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'job-offers-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'job-offers';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
?>
<section class="guru-block block__job-offers padding__section <?php echo esc_attr(' '.$className); ?>" id="open-jobs" >
        <div class="container-fluid wrap">
            <?php if(get_field('jobs_subline')): ?>
                <h4 class="font-size__small--x pretitle text-color__orange without-margin__bottom"><?php the_field('jobs_subline'); ?></h4>
            <?php endif; ?>

            <?php if(get_field('jobs_title')): ?>
                <h2 class="text-color__titles"><?php the_field('jobs_title'); ?></h2>
            <?php endif; ?>

            <?php if(get_field('jobs_text')): ?>
                <div class="font-size__medium"><?php the_field('jobs_text'); ?></div>
            <?php endif; ?>

            <?php
                $args = (array(
                   'post_type' => 'guru_job',
                   'posts_per_page' => -1,
                ) );
                $query = new WP_Query($args);
                if($query->have_posts()) :
             ?>

            <nav class="jobs__categories">
                <ul>
                    <li>
                        <a href="#" class="category--active" data-cat="job__post"><?php _e('All', 'gh-apollo'); ?></a>
                    </li>
                    <?php
                        $cats = get_terms( array( 'taxonomy' => 'guru_job_offer_type', 'hide_empty' => 0 ) );
                        foreach ($cats as $cat):
                    ?>
                        <li>
                            <a href="#" data-cat="<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </nav>
            <?php endif; ?>

            <div class="jobs__listing">
                <div class="row">
                    <?php
                        if($query->have_posts()) :
                    ?>
                        <?php
                            while($query->have_posts()) : $query->the_post();
                            $post__cats = wp_get_post_terms( get_the_ID() , 'guru_job_offer_type');
                        ?>
                        <article class="job__post job__post--active col-xs-12 col-sm-6 col-md-4 <?php foreach($post__cats as $post__cat) echo $post__cat->slug; ?>">
                            <h5 class="text-color__orange"><?php echo $post__cats[0]->name; ?></h5>
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <a href="<?php the_permalink(); ?>" class="btn--text"><?php _e('Apply now', 'gh-apollo'); ?></a>
                        </article>
                    <?php endwhile; else: ?>
                    <div class="background-color__utilitary border-radius__normal padding__mega margin-top__mega--x">
                        <h5><?php _e('No open jobs have been found. Stay alert!', 'understrap'); ?></h5>
                    </div>

                    <?php endif; ?>
                </div>
            </div>
        </div>
</section>

<?php if (is_admin()) : ?>

<div class="alert alert-warning">
    La visualización estará disponible cuando se hayan completado los campos en la columna derecha.
</div>

<?php endif; ?>
