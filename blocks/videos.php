<?php
/**
 * Steps Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'videos-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'videos';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
?>

<div class="block__videos <?php echo esc_attr(' '.$className); ?>" id="<?php echo esc_attr($id); ?>">
    <?php $i=1; if(have_rows('videos')) : while(have_rows('videos')): the_row(); ?>
        <div class="video-subsection" id="video-<?php echo $i; ?>">
            <div class="container-fluid wrap">
                <div class="row">
                    <div class="col-xs-2 col-sm-1">
                        <i class="far fa-play-circle font-size__mega--x text-color__orange"></i>
                    </div>

                    <div class="col-xs-10 col-sm-11 col-sm-8 col-md-4 start-xs">
                        <?php if(get_sub_field('pretitle')): ?>
                        <h4 class="font-size__small--x pretitle text-color__orange without-margin__bottom"><?php the_sub_field('pretitle'); ?></h4>
                        <?php endif; ?>

                        <?php if(get_sub_field('title')): ?>
                            <h2 class="text-color__titles"><?php the_sub_field('title'); ?></h2>
                        <?php endif; ?>

                        <?php if(get_sub_field('text')): ?>
                            <div class="font-size__medium"><?php the_sub_field('text'); ?></div>
                        <?php endif; ?>

                        <?php if(get_sub_field('cta_link')): ?>
                            <a href="<?php the_sub_field('cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__mega--x background-color__main text-transform__uppercase letter-spacing__medium font-weight__medium text-color__white padding__medium--x display__inline--block margin-top__mega banner-button">
                            <?php the_sub_field('cta_title'); ?>
                        </a>
                        <?php endif; ?>


                    </div>

                    <div class="col-xs-12 col-md-6 col-md-offset-1">
                        <div class="video-embed border-radius__normal">
                            <?php the_sub_field('embed'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php $i++; endwhile; endif; ?>
</div>

<?php if (is_admin()) : ?>

<div class="alert alert-warning">
    La visualización estará disponible cuando se hayan completado los campos en la columna derecha.
</div>

<?php endif; ?>
