<?php
/**
 * CTA Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'cta-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cta';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
?>

<section class="padding-top__small-section padding-bottom__small-section background-color__grey guru-block block__cta <?php the_field('color_combination'); echo esc_attr(' '.$className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container-fluid wrap">
        <div class="row middle-xs center-xs">
            <div class="col-xs-11">

                <?php if (get_field('cta_title_img')): ?>

                    <img src="<?php $img = get_field('cta_title_img'); echo $img['sizes']['medium_large']; ?>">

                <?php elseif(get_field('cta_title')): ?>
                    <h2 class="font-size__mega"><?php the_field('cta_title'); ?></h2>
                <?php endif; ?>

                <?php if(get_field('cta_text')): ?>
                    <?php the_field('cta_text'); ?>
                <?php endif; ?>

                <footer>
                    <a href="<?php the_field('cta_cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__mega--x background-color__main text-transform__uppercase letter-spacing__medium font-weight__medium text-color__white padding__medium--x display__inline--block margin-top__mega banner-button">
                        <?php the_field('cta_cta_text'); ?>
                    </a>

                    <?php if(get_field('intro_cta_secondary_text')): ?>
                        <a href="<?php the_field('cta_cta_secondary_link'); ?>" class="text-color__text btn--text margin-left__mega--x display__inline--block" data-lity><i class="fa fa-play-circle margin-right__normal"></i><?php the_field('cta_cta_secondary_text'); ?></a>
                    <?php endif; ?>
                </footer>
            </div>
        </div>
    </div>
</section>

<?php if (is_admin()) : ?>

<div class="alert alert-warning">
    La visualización estará disponible cuando se hayan completado los campos en la columna derecha.
</div>

<?php endif; ?>
