<?php
/**
 * Steps Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'steps-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'steps';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
?>

<section class="padding__section text-color__white guru-block block__steps padding__section <?php if(get_field('deco')) echo 'guru-block--deco '; the_field('color_combination'); echo esc_attr(' '.$className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="home-steps__wrapper background-color__titles">
        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-xs-11 col-md-10 col-sm-6">
                    <?php if(get_field('steps_pretitle')): ?>
                        <h4 class="font-size__small--x pretitle text-color__orange without-margin__bottom"><?php the_field('steps_pretitle'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('steps_title')): ?>
                        <h2 class="text-color__white without-margin__top"><?php the_field('steps_title'); ?></h2>
                    <?php endif; ?>
                </div>
                <div class="col-sm-12"></div>
                <div class="col-xs-11 col-sm-5">
                    <?php if(get_field('steps_text')): ?>
                        <div class="font-size__medium"><?php the_field('steps_text'); ?></div>
                    <?php endif; ?>
                </div>
            </div>

            <?php if(have_rows('steps')) : ?>

            <div class="row padding-top__small-section middle-xs">
                <div class="col-xs-8 col-xs-offset-3 col-sm-offset-3 col-sm-7 col-md-4 col-md-offset-2">
                    <div class="steps__list">
                        <?php
                            $steps_count = 0;
                            while(have_rows('steps')): the_row();
                        ?>
                            <div class="steps__step <?php if ($steps_count == '') echo 'steps__step--active'; ?>" data-target="<?php echo $steps_count; ?>">
                                <span class="icon padding__medium--x background-color__white display__inline--block">
                                    <?php echo $steps_count+1; ?>
                                </span>
                                <h4 class="text-color__white font-size__medium margin-bottom__medium"><?php the_sub_field('title'); ?></h4>
                                <p><?php the_sub_field('text'); ?></p>
                            </div>
                        <?php $steps_count++; endwhile; ?>
                    </div>
                </div>

                <div class="col-xs-12 col-md-5 col-md-offset-1">
                    <div class="steps__illustrations">
                        <?php while(have_rows('steps')): the_row(); ?>
                            <div class="steps__illustration">
                                <img src="<?php $img = get_sub_field('image'); echo $img['sizes']['large']; ?>">
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>

        <?php endif; ?>

        <?php if(get_field('steps_cta_link')): ?>
            <div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-1">
                <a href="<?php the_field('steps_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('steps_cta_text'); ?></a>
            </div>
        <?php endif; ?>

        </div>
    </div>
</section>

<?php if (is_admin()) : ?>

<div class="alert alert-warning">
    La visualización estará disponible cuando se hayan completado los campos en la columna derecha.
</div>

<?php endif; ?>
