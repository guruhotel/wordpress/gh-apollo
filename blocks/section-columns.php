<?php
/**
 * Section Columns Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'section-columns-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'section-columns';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

// Load values and assign defaults.
?>
<section class="guru-block block__section-columns padding__section <?php if(get_field('bottom_deco')) echo 'guru-block--deco '; if(get_field('title_inline')) echo 'guru-block--title-inline '; the_field('color_combination'); echo esc_attr(' '.$className); ?>" id="<?php echo esc_attr($id); ?>" >

        <?php if(get_field('columns_bg')): ?>
            <img src="<?php $img = get_field('columns_bg'); echo $img['url']; ?>" class="guru-block__cover-bg">
        <?php endif; ?>

        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <?php if(get_field('columns_subline')): ?>
                        <h4 class="font-size__small--x pretitle text-color__orange without-margin__bottom"><?php the_field('columns_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('columns_title')): ?>
                        <h2 class="text-color__titles"><?php the_field('columns_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('columns_text')): ?>
                        <div class="font-size__medium"><?php the_field('columns_text'); ?></div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="block__section-columns__columns">
                <?php if(have_rows('columns')) : ?>
                    <div class="row">
                        <?php while(have_rows('columns')): the_row(); ?>
                        <article class="item <?php if(get_field('columns_offset')) echo 'col-xs-12 col-md-3 col-md-offset-1 col-sm-4'; else echo 'col-xs-12 col-sm'; ?>">
                            <?php if(get_sub_field('icon')): ?>
                                <i class="icon--circle <?php the_sub_field('icon'); ?>"></i>
                            <?php endif; ?>

                            <?php if(get_sub_field('title')): ?>
                                <h4 class="font-size__medium margin-top__medium--x"><?php the_sub_field('title'); ?></h4>
                            <?php endif; ?>

                            <?php if(get_sub_field('text')): ?>
                                <?php the_sub_field('text'); ?>
                            <?php endif; ?>
                        </article>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="center-xs">
                <?php if(get_field('columns_cta_link')): ?>
                    <a href="<?php the_field('columns_cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__mega--x background-color__main text-transform__uppercase letter-spacing__medium font-weight__medium text-color__white padding__medium--x display__inline--block banner-button">
                        <?php the_field('columns_cta_text'); ?>
                    </a>
                <?php endif; ?>
            </div>

        </div>
</section>

<?php if (is_admin()) : ?>

<div class="alert alert-warning">
    La visualización estará disponible cuando se hayan completado los campos en la columna derecha.
</div>

<?php endif; ?>
