<?php
/**
 * Template Name: About us
 */
get_header();
while(have_posts()): the_post();
?>
    <div class="section-intro background-color__main padding-bottom__small-section padding-top__section">

        <div class="current-webpage-widget text-color__white hidden__xs">
            <div class="current-webpage-widget__inner border-radius__normal">
                <ul>
                    <li <?php if (is_page_template('page-templates/about-us.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1286); ?>"><i class="fas fa-users"></i> About Us</a>
                    </li>
                    <li <?php if (is_page_template('page-templates/careers.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1209); ?>"><i class="fas fa-clipboard-list"></i> Careers</a>
                    </li>
                    <li <?php if (is_page_template('page-templates/affiliates.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1119); ?>"><i class="fas fa-project-diagram"></i> Affiliates</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-11 col-md-5 text-color__white">

                    <?php if(get_field('about_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white line-height__medium--x" data-aos="fade-up" data-aos-delay="200"><?php the_field('about_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('about_title')): ?>
                        <h1 class="font-size__mega--x text-color__white" data-aos="fade-up" data-aos-delay="200"><?php the_field('about_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('about_text')): ?>
                        <div data-aos="fade-up" data-aos-delay="400">
                            <?php the_field('about_text'); ?>
                        </div>
                    <?php endif; ?>

                    <footer data-aos="fade-up" data-aos-delay="600">
                       <?php if(get_field('about_intro_cta_link')): ?>
                           <a href="<?php the_field('about_intro_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('about_intro_cta_text'); ?></a>
                       <?php endif; ?>
                    </footer>
                </div>
            </div>
        </div>
    </div>

    <section class="about-content padding__section background-color__white">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-10 col-md-5 start-xs">
                    <?php if(get_field('about_content_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__titles line-height__medium--x" data-aos="fade-up" data-aos-delay="200"><?php the_field('about_content_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('about_content_title')): ?>
                        <h1 class="font-size__mega--x text-color__titles" data-aos="fade-up" data-aos-delay="200"><?php the_field('about_content_title'); ?></h1>
                    <?php endif; ?>
                </div>
                <div class="col-xs-10 col-md-6 col-md-offset-1 start-xs">
                    <?php if(get_field('about_content_text')): ?>
                        <div data-aos="fade-up" data-aos-delay="400">
                            <?php the_field('about_content_text'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="about-stats padding-top__section padding-bottom__small-section">
        <div class="container-fluid wrap center-xs">
            <?php if(get_field('about_stats_subline')): ?>
               <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__titles" data-aos="fade-up"><?php the_field('about_stats_subline'); ?></h4>
            <?php endif; ?>

            <?php if(get_field('about_stats_title')): ?>
                <h1 class="font-size__mega text-color__titles" data-aos="fade-up"><?php the_field('about_stats_title'); ?></h1>
            <?php endif; ?>

            <?php if(get_field('about_stats_text')): ?>
                <p data-aos="fade-up">
                    <?php the_field('about_stats_text'); ?>
                </p>
            <?php endif; ?>

            <div class="row margin-top__mega--x">
                <?php $stat_count = 0; if(have_rows('about_stats')) : while(have_rows('about_stats')): the_row(); ?>
                    <article class="margin-bottom__big col-xs-6 col-md-3" data-aos="fade-up">
                        <div class="card border-radius__medium box-shadow__normal background-color__white center-xs text-color__white">
                            <i class="icon text-color__main display__inline--block <?php the_sub_field('icon'); ?>"></i>
                            <h4 class="font-size__mega text-color__titles display__inline--block"><?php the_sub_field('title'); ?></h4>

                            <span class="price font-size__medium text-color__titles display__block"><?php the_sub_field('subtitle'); ?></span>
                        </div>
                    </article>
                <?php $stat_count++; endwhile; endif; ?>
            </div>
        </div>
    </section>

    <section class="about-team background-color__main padding__small-section">
        <div class="container-fluid wrap center-xs text-color__white">
            <?php if(get_field('about_stats_subline')): ?>
               <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white" data-aos="fade-up"><?php the_field('about_stats_subline'); ?></h4>
            <?php endif; ?>

            <?php if(get_field('about_team_title')): ?>
                <h2 class="font-size__mega text-color__white" data-aos="fade-up"><?php the_field('about_team_title'); ?></h2>
            <?php endif; ?>

            <?php if(get_field('about_team_text')): ?>
                <?php the_field('about_team_text'); ?>
            <?php endif; ?>

            <div class="about-team__members margin-top__mega--x">
                <?php
                   $args = (array(
                       'post_type' => 'guru_member',
                       'posts_per_page' => -1,
                   ) );
                   $query = new WP_Query($args);
                   if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
               ?>
                    <article>
                        <?php the_post_thumbnail('medium'); ?>
                        <h4 class="text-color__white"><?php the_title(); ?></h4>
                        <span><?php the_field('member_role'); ?></span>
                    </article>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>

            <?php
                if(get_field('about_team_logos')):
                    $images = get_field('about_team_logos');
            ?>
                <div class="row center-xs">
                    <div class="col-md-10">
                        <div class="about-team__logos background-color__white padding__medium box-shadow__normal border-radius__normal">
                            <div class="row middle-xs center-xs">
                                <?php foreach ($images as $img): ?>
                                    <article class="col-xs-4 col-md">
                                        <img src="<?php echo $img['sizes']['medium']; ?>">
                                    </article>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>

    <section class="about-investors padding-top__section">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-11 col-md-5 start-xs">
                    <?php if(get_field('about_investors_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__titles line-height__medium--x"><?php the_field('about_investors_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('about_investors_title')): ?>
                        <h1 class="font-size__mega--x text-color__titles"><?php the_field('about_investors_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('about_investors_text')): ?>
                        <div>
                            <?php the_field('about_investors_text'); ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-xs-11 col-md-7">
                    <div class="row center-xs">
                        <?php if(have_rows('about_investors_blocks')) : while(have_rows('about_investors_blocks')): the_row(); ?>
                            <div class="col-xs-10 col-sm-6">
                                <article class="card background-color__white box-shadow__normal border-radius__normal padding__mega">
                                    <img src="<?php $img = get_sub_field('logo'); echo $img['sizes']['medium']; ?>">
                                    <h4 class="text-color__titles"><?php the_sub_field('title'); ?></h4>
                                    <?php the_sub_field('desc'); ?>
                                </article>
                            </div>
                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about-press padding-top__small-section">
        <div class="container-fluid wrap">

            <?php if(get_field('about_press_title')): ?>
                <h2 class="font-size__mega--x text-color__titles margin-bottom__mega--x"><?php the_field('about_press_title'); ?></h2>
            <?php endif; ?>

            <div class="row">
                <?php
                   $press_qty = get_field('about_press_qty');
                   $args = (array(
                       'post_type' => 'guru_press',
                       'posts_per_page' => $press_qty,
                   ) );
                   $query = new WP_Query($args);
                   if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
               ?>
                <div class="col-xs-12 col-sm-4">
                    <div class="about-press__item card background-color__white border-radius__normal box-shadow__medium margin-bottom__big">

                        <div class="about-press__item__thumb background-color__main">
                            <a href="<?php the_permalink( ); ?>">
                                <?php the_post_thumbnail('medium_large'); ?>
                            </a>
                        </div>

                        <div class="about-press__item__content padding__mega">
                            <time class="meta font-size__small--x font-weight__normal text-color__text"><i class="far fa-calendar text-color__main" aria-hidden="true"></i>
                                    <time><?php echo get_the_date(); ?></time></time>

                            <a href="<?php the_permalink(); ?>"><h2 class="article-title font-size__medium margin-bottom__normal"><?php the_title(); ?></h2></a>

                            <a href="<?php the_field('external_link'); ?>" class="btn__read font-weight__normal font-size__small--x text-color__main" target="_blank"><?php _e('Leer', 'apollo'); ?><i class="fas fa-long-arrow-alt-right margin-left__normal" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>
    </section>


    <section id="home__cta" class="padding-top__section padding-bottom__mega--x background-color__grey">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-10 col-md-6">

                    <?php if (get_field('about_cta_title_img')): ?>

                        <img src="<?php $img = get_field('about_cta_title_img'); echo $img['sizes']['medium_large']; ?>">

                    <?php elseif(get_field('about_cta_title')): ?>
                        <h2 class="font-size__mega"><?php the_field('about_cta_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('about_cta_text')): ?>
                        <?php the_field('about_cta_text'); ?>
                    <?php endif; ?>

                    <?php if(get_field('about_cta_link')): ?>
                        <a href="<?php the_field('about_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('about_cta_cta_text'); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; get_footer();
