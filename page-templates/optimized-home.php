<?php
/**
 * Template Name: Optimized Home
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    <section class="home__main-banner padding-bottom__section">
        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-xs-12 col-md-11">
                    <?php if(get_field('intro_title_first_part')): ?>
                       <h1 class="banner-title"><?php the_field('intro_title_first_part'); ?> <span class="text-color__main display__block"><?php the_field('intro_title_second_part'); ?></span></h1>
                    <?php endif; ?>
                </div>

                <div class="col-xs-12 col-md-6">
                    <?php if(get_field('intro_text')): ?>
                       <p class="banner-desc font-size__medium"><?php the_field('intro_text'); ?></p>
                   <?php endif; ?>

                   <?php if(get_field('intro_extra_text')): ?>
                        <div class="banner-extra-text margin-top__mega--x">
                           <?php the_field('intro_extra_text'); ?>
                       </div>
                   <?php endif; ?>

                   <?php if(get_field('intro_cta_text')): ?>
                        <footer>
                             <a href="<?php the_field('intro_cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__mega--x background-color__main text-transform__uppercase letter-spacing__medium  font-weight__medium text-color__white padding__medium--x display__inline--block margin-top__mega  banner-button">
                                <?php the_field('intro_cta_text'); ?>
                            </a>

                            <?php if(get_field('intro_cta_secondary_text')): ?>
                                <a href="<?php the_field('intro_cta_secondary_link'); ?>" class="text-color__text btn--text margin-left__mega--x display__inline--block" data-lity><i class="fa fa-play-circle margin-right__normal"></i><?php the_field('intro_cta_secondary_text'); ?></a>
                            <?php endif; ?>

                            <?php if(get_field('intro_cta_text_secondary')): ?>
                                <span><?php the_field('intro_cta_text_secondary'); ?></span>
                            <?php endif; ?>
                        </footer>

                   <?php endif; ?>
                </div>

                <?php if(get_field('intro_integrations_logos')) : ?>
                    <div class="col-xs-12 col-md-12">
                        <div class="banner-integrations">
                            <hr>

                            <div class="banner-integrations__logos">
                                <div class="row middle-xs">
                                    <?php
                                        $logo_count = 1;
                                        $integrations_logos = get_field('intro_integrations_logos');
                                        foreach ($integrations_logos as $logo):
                                    ?>
                                        <?php if ($logo_count == 7): ?>
                                            <div class="col-xs-12"></div>
                                        <?php endif ?>

                                        <div class="logo-animated <?php if ($logo_count >= 7) { echo 'logo-initial-hidden'; } ?>">
                                            <img src="<?php echo $logo['sizes']['medium']; ?>">
                                        </div>
                                    <?php $logo_count++; endforeach; ?>
                                </div>
                            </div>

                            <hr>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <?php if(have_rows('intro_features')) : ?>
            <div class="banner-features">
                <div class="banner-features__slider">
                    <?php
                        $features_count = 1;
                        while(have_rows('intro_features')): the_row();
                    ?>
                        <div class="banner-features__card">
                            <?php if(get_sub_field('video')): ?>
                                <div class="video-container">
                                    <video src="<?php the_sub_field('video'); ?>" <?php if ($features_count == 1) echo 'autoplay'; ?> muted playsinline loop></video>
                                </div>
                            <?php else: ?>
                                <img src="<?php $img = get_sub_field('image'); echo $img['sizes']['medium_large']; ?>">
                            <?php endif; ?>

                            <div class="banner-features__card--body margin-top__big margin-left__medium--x">
                                <h5 class="margin-bottom__normal"><?php the_sub_field('title'); ?></h5>
                                <?php the_sub_field('text'); ?>
                            </div>
                        </div>
                    <?php $features_count++; endwhile; ?>
                </div>
            </div>
        <?php endif; ?>
    </section>

    <section id="home-steps" class="padding__section text-color__white">
        <div class="home-steps__wrapper background-color__titles">
            <div class="container-fluid wrap">
                <div class="row">
                    <div class="col-xs-11 col-md-10 col-sm-6">
                        <?php if(get_field('steps_pretitle')): ?>
                            <h4 class="font-size__small--x pretitle text-color__orange without-margin__bottom"><?php the_field('steps_pretitle'); ?></h4>
                        <?php endif; ?>

                        <?php if(get_field('steps_title')): ?>
                            <h2 class="text-color__white without-margin__top"><?php the_field('steps_title'); ?></h2>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-12"></div>
                    <div class="col-xs-11 col-sm-5">
                        <?php if(get_field('steps_text')): ?>
                            <div class="font-size__medium"><?php the_field('steps_text'); ?></div>
                        <?php endif; ?>
                    </div>
                </div>

                <?php if(have_rows('steps')) : ?>

                <div class="row padding-top__small-section middle-xs">
                    <div class="col-xs-8 col-xs-offset-3 col-sm-offset-3 col-sm-7 col-md-4 col-md-offset-2">
                        <div class="steps__list">
                            <?php
                                $steps_count = 0;
                                while(have_rows('steps')): the_row();
                            ?>
                                <div class="steps__step <?php if ($steps_count == '') echo 'steps__step--active'; ?>" data-target="<?php echo $steps_count; ?>">
                                    <span class="icon padding__medium--x background-color__white display__inline--block">
                                        <?php echo $steps_count+1; ?>
                                    </span>
                                    <h4 class="text-color__white font-size__medium margin-bottom__medium"><?php the_sub_field('title'); ?></h4>
                                    <p><?php the_sub_field('text'); ?></p>
                                </div>
                            <?php $steps_count++; endwhile; ?>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-5 col-md-offset-1">
                        <div class="steps__illustrations">
                            <?php while(have_rows('steps')): the_row(); ?>
                                <div class="steps__illustration">
                                    <img src="<?php $img = get_sub_field('image'); echo $img['sizes']['large']; ?>">
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>

            <?php endif; ?>

            <?php if(get_field('steps_cta_link')): ?>
                <div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-1">
                    <a href="<?php the_field('steps_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('steps_cta_text'); ?></a>
                </div>
            <?php endif; ?>

            </div>
        </div>
    </section>

    <?php if (get_field('show_quotes')): ?>

        <section id="home-quotes" class="position__relative">
            <div class="container-fluid wrap">
                <div class="row middle-xs">
                    <div class="col-xs-12">
                        <h2><?php the_field('quotes_title'); ?></h2>
                    </div>
                </div>
            </div>

            <div class="quotes-slider">
                <?php
                    $i = 1;
                    $args = array(
                      'post_type'      => 'guru_case_study',
                      'posts_per_page' => 8,
                    );
                    $query = new WP_Query( $args );
                    while($query->have_posts()) : $query->the_post();
                ?>
                    <div class="quotes-slider__slide center-xs margin-bottom__big">
                        <?php the_post_thumbnail('medium_large', array('class'=> 'quotes-slider__img--hotel')); ?>

                        <div class="background-color__white border-radius__big">

                            <blockquote class="position__relative">
                                <?php echo strip_tags(get_the_content()); ?>
                            </blockquote>

                            <div class="margin-top__mega--x">
                                <h6 class="font-size__medium margin-bottom__small"><?php the_field('name'); ?></h6>
                                    <?php the_field('titulo'); ?>
                                    <?php if(get_field('hotel_url')): ?>
                                        <a href="#" class="text-color__text font-size__small--x font-weight__normal"><?php $hotel_url = get_field('hotel_url'); echo parse_url($hotel_url, PHP_URL_HOST); ?></a>
                                    <?php endif; ?>
                            </div>

                            <?php if (have_rows('case_stats')): ?>
                                <footer class="margin-top__mega--x">
                                    <div class="row">
                                        <?php while(have_rows('case_stats')): the_row(); ?>
                                            <article class="item col-xs">
                                                <i class="text-color__main margin-right__small font-size__medium <?php the_sub_field('icon'); ?>"></i>
                                                <span class="text-color__titles font-size__big--x font-weight__medium vertical-align__bottom line-height__normal"><?php the_sub_field('number'); ?></span>
                                                <p class="font-size__small--x font-weight__medium"><?php the_sub_field('text'); ?></p>
                                            </article>
                                        <?php endwhile; ?>
                                    </div>
                                </footer>
                            <?php endif ?>
                        </div>

                    </div>
               <?php endwhile; wp_reset_postdata(); ?>
            </div>

            <div class="container-fluid wrap">
                <div class="row center-xs">
                    <div class="quotes-logos margin-top__mega--x center-xs">
                        <div class="row middle-xs">
                            <?php if(have_rows('quotes_logos')) : while(have_rows('quotes_logos')): the_row(); ?>
                                <a href="<?php the_sub_field('url'); ?>" target="_blank">
                                    <img src="<?php $img = get_sub_field('logo'); echo $img['sizes']['medium']; ?>">
                                </a>
                            <?php endwhile; endif; ?>
                        </div>
                    </div>

                    <?php if(get_field('quotes_footer_text')): ?>
                        <div class="col-xs-11 col-md-8 center-xs margin-top__mega--x font-size__small--x font-weight__medium text-color__titles">
                            <?php the_field('quotes_footer_text'); ?>

                            <?php if(get_field('quotes_footer_cta_link')): ?>
                                 <a href="<?php the_field('quotes_footer_cta_link'); ?>" class="btn--primary background-color__titles text-color__white margin-top__big border-radius__mega--x padding__medium display__inline--block padding-right__mega--x padding-left__mega--x"><?php the_field('quotes_footer_cta_text'); ?></a>
                            <?php endif; ?>
                        </div>

                    <?php endif; ?>

                </div>
            </div>
        </section>
    <?php endif; ?>

    <section class="home-features">
        <div class="home-features__wrapper background-color__white padding__section ">
            <div class="container-fluid wrap">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <?php if(get_field('guru_features_pretitle')): ?>
                            <h4 class="font-size__small--x pretitle text-color__main"><?php the_field('guru_features_pretitle'); ?></h4>
                        <?php endif; ?>

                        <?php if(get_field('guru_features_title')): ?>
                            <h2><?php the_field('guru_features_title'); ?></h2>
                        <?php endif; ?>

                        <?php if(get_field('guru_features_text')): ?>
                            <?php the_field('guru_features_text'); ?>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row">
                    <?php $i = 0; if(have_rows('guru_features_features')) : while(have_rows('guru_features_features')): the_row(); ?>
                        <?php if ($i % 3 == 0): ?>
                            <div class="col-md-12"></div>
                        <?php endif ?>
                        <article class="item col-xs-12 col-md-3">
                            <i class="text-color__main icon <?php the_sub_field('icon'); ?>"></i>
                            <h5 class="font-size__medium margin-bottom__medium"><?php the_sub_field('title'); ?></h5>
                            <?php the_sub_field('text'); ?>
                        </article>
                    <?php $i++; endwhile; endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section id="home__cta" class="padding-top__small-section padding-bottom__small-section background-color__grey">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-11">

                    <?php if (get_field('cta_title_img')): ?>

                        <img src="<?php $img = get_field('cta_title_img'); echo $img['sizes']['medium_large']; ?>">

                    <?php elseif(get_field('cta_title')): ?>
                        <h2 class="font-size__mega"><?php the_field('cta_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('cta_text')): ?>
                        <?php the_field('cta_text'); ?>
                    <?php endif; ?>

                    <footer>
                        <a href="<?php the_field('cta_cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__mega--x background-color__main text-transform__uppercase letter-spacing__medium font-weight__medium text-color__white padding__medium--x display__inline--block margin-top__mega banner-button">
                            <?php the_field('cta_cta_text'); ?>
                        </a>

                        <?php if(get_field('intro_cta_secondary_text')): ?>
                            <a href="<?php the_field('cta_cta_secondary_link'); ?>" class="text-color__text btn--text margin-left__mega--x display__inline--block" data-lity><i class="fa fa-play-circle margin-right__normal"></i><?php the_field('cta_cta_secondary_text'); ?></a>
                        <?php endif; ?>
                    </footer>
                </div>
            </div>
        </div>
    </section>

    <?php if(get_field('covid_text')): ?>
        <section id="home__covid" class="background-color__white padding__small-section">

            <div class="container-fluid wrap">
                <div class="row center-xs">
                    <div class="col-xs-12 col-md-8 col-lg-8">
                        <div class="card border-color__utilitary border-radius__normal padding__medium--x ">
                            <div class="row">
                                <div class="col-xs-3">
                                    <div class="border-radius__normal center-xs home__covid--icon">
                                        <img src="<?php $img = get_field('covid_image'); echo $img['sizes']['medium']; ?>">
                                    </div>
                                </div>
                                <div class="col-xs-8">
                                    <?php if(get_field('covid_text')): ?>
                                        <p class="text-color__titles"><?php the_field('covid_text'); ?></p>
                                    <?php endif; ?>

                                    <?php if(get_field('covid_link_url')): ?>
                                        <a href="<?php the_field('covid_link_url'); ?>" class="btn--text text-color__red margin-top__normal margin-bottom__medium "><?php the_field('covid_link_text'); ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

<?php endwhile; get_footer();
