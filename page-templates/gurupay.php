<?php
/* Template name: GuruPay */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="page-template-landing">
    <div class="background-color__titles hero-page hero-two">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-10 col-sm-10 col-md-8">
                    <div class="hero-caption">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/paybyguru.svg"class="margin-bottom__medium gurupay-logo">

                        <h1 class="font-size__mega--x text-color__white without-margin-top" data-aos="fade-up"><?php the_title(); ?></h1>

                        <?php if(get_field('landing_subline')): ?>
                            <p class="text-color__white font-size__medium margin-top__medium" data-aos="fade-up" data-aos-delay="0.2s"><?php the_field('landing_subline'); ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if(have_rows('landing_main_features')) : ?>
        <div class="container-fluid wrap template-landing-main-features margin-top__mega--x margin-bottom__mega--x">
            <div class="row center-xs">
                <?php while(have_rows('landing_main_features')): the_row(); ?>
                    <div class="feature col-xs-12 col-sm-8 col-md-4">
                        <div class="card padding__big border-color__grey--regent box-shadow__medium start-xs">
                            <i class="<?php the_sub_field('icon'); ?> icon font-size-x-medium main-color"></i>
                            <h3 class="font-size__medium margin-top__normal">
                                <?php the_sub_field('title'); ?>
                            </h3>
                            <p class="text-color__titles"><?php the_sub_field('text'); ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="container-fluid wrap">
        <div class="row center-xs">
            <div class="col-xs-11 col-md-12 start-xs">
                <div class="the-content"><?php the_content(); ?></div>
            </div>
        </div>
    </div>
    <?php
    $cta_title = get_field('landing_cta_title');
    $cta_text = get_field('landing_cta_text');
    $cta_btn_text = get_field('landing_cta_btn_text');
    $cta_btn_url = get_field('landing_cta_btn_url');
    if($cta_title || $cta_text) { ?>
        <div class="page-cta background-color__titles margin-top__mega--x padding-top__big--x padding-bottom__big--x">
            <div class="container-fluid wrap">
                <div class="row center-xs">
                    <div class="col-xs-12 col-md-8 center-xs">
                        <?php if( $cta_title ) { ?>
                            <h2 class="line-height__big text-color__yellow"><?php echo $cta_title; ?></h2>
                        <?php } ?>
                        <?php if( $cta_text ) { ?>
                            <p class="font-size__big text-color__white margin-bottom__big--x"><?php echo $cta_text; ?></p>
                        <?php } ?>
                        <?php if( $cta_btn_text ) { ?>
                            <a href="<?php echo $cta_btn_url; ?>" class="btn background-color__yellow border-radius__small--x font-size__medium btn__size--medium text-color__titles" data-aos="fade-up" data-aos-delay="0.4s"><?php echo $cta_btn_text; ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php
get_footer();
