<?php
/**
 * Template Name: Product - Marketplace
 */
get_header();
while(have_posts()): the_post();
?>

    <div class="background-color__main padding__section text-color__white section-intro">

        <div class="current-webpage-widget text-color__white hidden__xs">
            <div class="current-webpage-widget__inner border-radius__normal">
                <ul>
                    <li <?php if (is_page_template('page-templates/website.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1250); ?>"><i class="fas fa-globe-americas"></i> Website</a>
                    </li>
                    <li <?php if (is_page_template('page-templates/checkout.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1209); ?>"><i class="fas fa-shopping-cart"></i> Checkout</a>
                    </li>
                    <li <?php if (is_page_template('page-templates/marketplace.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1119); ?>"><i class="fas fa-toggle-on"></i> Marketplace</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-6">

                    <?php if(get_field('marketplace_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white" data-aos="fade-up" data-aos-delay="200"><?php the_field('marketplace_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('marketplace_title')): ?>
                        <h1 class="font-size__mega text-color__white"><?php the_field('marketplace_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('marketplace_text')): ?>
                        <?php the_field('marketplace_text'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php
        $rest_url = 'https://back.guruhotel.com/marketplaces/';
        $results = file_get_contents($rest_url);
        $results = json_decode($results);
        $categories = array();

        //Get categories
        foreach($results as $result){
           if (!in_array($result->category->title, $categories)){
               $categories[] = $result->category->title;
           }
        }
    ?>

    <?php foreach ($categories as $category): ?>

        <div class="marketplace-cards padding-top__small-section">
            <div class="container-fluid wrap">
                <div class="row">
                    <div class="col-md-6">
                        <h2><?php echo $category; ?></h2>
                    </div>
                </div>

                <div class="row margin-top__mega">
                    <?php
                        $current_language = get_locale();
                        foreach($results as $result):
                            if($result->category->title == $category):
                    ?>
                        <div class="col-xs-12 col-md-4">
                            <div class="card background-color__white border-radius__normal box-shadow__medium display__inline--block margin-bottom__big" data-aos="fade-up">
                                <img src="<?php echo $result->image->url; ?>" alt="<?php echo $result->name; ?> Logo">
                                <h2 class="font-size__medium"><?php echo $result->name; ?></h2>
                                <?php if( $current_language == 'en_US' ): ?>
                                    <p class="margin-bottom__big"><?php echo $result->description; ?></p>
                                <?php else: ?>
                                    <p class="margin-bottom__big"><?php echo $result->description_es; ?></p>
                                <?php endif; ?>
                                <!-- <span class="text-color__titles display__block"><?php //echo $result->price_description; ?></span> -->
                                <a href="https://app.guruhotel.com/register?ref=landing-marketplace" class="btn  btn--full-width border-radius__normal background-color__main text-color__white display__block center-xs padding__normal display__inline--block margin-top__normal font-size__small--x" target="_blank"><?php _e('Install', 'gh-apollo'); ?></a>

                                <?php if (!empty($result->badge)): ?>
                                    <span class="app__badge" style="background-color: <?php echo $result->badge->color; ?>;"><?php echo $result->badge->text; ?></span>
                                <?php endif ?>

                            </div>
                        </div>
                    <?php endif; endforeach; ?>
                </div>
            </div>
        </div>

    <?php endforeach ?>

<?php endwhile; get_footer();
