<?php
/**
 * Template Name: Product - Checkout
 */
get_header();
while(have_posts()): the_post();
?>

    <div class="checkout-intro section-intro background-color__main padding-bottom__small-section padding-top__section">

        <div class="current-webpage-widget text-color__white hidden__xs">
            <div class="current-webpage-widget__inner border-radius__normal">
                <ul>
                    <li <?php if (is_page_template('page-templates/website.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1250); ?>"><i class="fas fa-globe-americas"></i> Website</a>
                    </li>
                    <li <?php if (is_page_template('page-templates/checkout.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1209); ?>"><i class="fas fa-shopping-cart"></i> Checkout</a>
                    </li>
                    <li <?php if (is_page_template('page-templates/marketplace.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1119); ?>"><i class="fas fa-toggle-on"></i> Marketplace</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container-fluid wrap">
            <div class="row center-xs middle-xs">
                <div class="col-xs-11 col-md-6 start-xs text-color__white">

                    <?php if(get_field('checkout_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white" data-aos="fade-up" data-aos-delay="200"><?php the_field('checkout_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('checkout_title')): ?>
                        <h1 class="font-size__mega text-color__white"><?php the_field('checkout_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('checkout_text')): ?>
                        <?php the_field('checkout_text'); ?>
                    <?php endif; ?>
                </div>

                <div class="col-xs-11 col-md-5 col-md-offset-1">
                    <div class="checkout-animation position__relative">
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/checkout.svg" alt="">
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/checkout-icon-1.svg"  data-aos="fade-up" data-aos-delay="200">
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/checkout-icon-2.svg"  data-aos="fade-down" data-aos-delay="400">
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/checkout-icon-3.svg"  data-aos="fade-up" data-aos-delay="600">
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/checkout-icon-4.svg"  data-aos="fade-down" data-aos-delay="800">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if(have_rows('checkout_features')) : ?>
        <div class="checkout-features padding-bottom__small-section">
            <div class="container-fluid wrap">
                <div class="row center-xs middle-xs">
                    <?php $features_count = 1; while(have_rows('checkout_features')): the_row(); ?>
                        <div class="col-xs-11 col-sm-6 col-md-4">
                            <div class="card box-shadow__normal start-xs border-radius__normal padding__big--x background-color__white margin-top__mega--x font-size__medium--x" data-aos="fade-up" data-aos-delay="<?php echo $features_count*200; ?>">
                                <div class="row middle-xs">
                                    <div class="col-xs-3 col-sm-4">
                                        <span class="padding__medium--x background-color__main border-radius__medium display__inline--block vertical-align__middle">
                                            <i class="<?php the_sub_field('icon'); ?> text-color__white font-size__mega"></i>
                                        </span>
                                    </div>
                                    <div class="col-xs-9 col-sm-8">
                                        <h4><?php the_sub_field('text'); ?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php $features_count++; endwhile; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if(have_rows('checkout_sections')) : ?>
        <div class="checkout-sections padding-bottom__section">
            <div class="container-fluid wrap">
                <?php $section_count = 0; while(have_rows('checkout_sections')): the_row(); ?>
                    <section>
                        <div class="row center-xs middle-xs">
                            <div class="col-xs-11 col-md-5 start-xs col-md-offset-1 <?php if($section_count%2 == 0) echo 'last-md'; ?>">
                                <?php if(get_sub_field('subline')): ?>
                                   <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase" data-aos="fade-up" data-aos-delay="200"><?php the_sub_field('subline'); ?></h4>
                                <?php endif; ?>

                                <?php if(get_sub_field('title')): ?>
                                    <h2 class="font-size__mega" data-aos="fade-up"><?php the_sub_field('title'); ?></h1>
                                <?php endif; ?>

                                <?php if(get_sub_field('text')): ?>
                                    <p data-aos="fade-up">
                                        <?php the_sub_field('text'); ?>
                                    </p>
                                <?php endif; ?>
                            </div>

                            <div class="col-xs-12 col-md-5 col-md-offset-1">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/checkout-section-<?php echo $section_count; ?>.svg" data-aos="zoom-in">
                            </div>
                        </div>
                    </section>
                <?php $section_count++; endwhile; ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="checkout-addons background-color__main text-color__white padding-top__section padding-bottom__section">
        <div class="container-fluid wrap">
            <div class="row center-xs middle-xs">
                <div class="col-xs-11 col-md-5 start-xs">

                    <?php if(get_field('checkout_addons_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white" data-aos="fade-up"><?php the_field('checkout_addons_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('checkout_addons_title')): ?>
                        <h1 class="font-size__mega text-color__white" data-aos="fade-up"><?php the_field('checkout_addons_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('checkout_addons_text')): ?>
                        <p data-aos="fade-up">
                            <?php the_field('checkout_addons_text'); ?>
                        </p>
                    <?php endif; ?>
                </div>

                <div class="col-xs-11 col-md-6 col-md-offset-1 start-xs">
                    <?php $addon_count = 0; if(have_rows('checkout_addons')) : while(have_rows('checkout_addons')): the_row(); ?>
                        <article class="card background-color__white border-radius__medium box-shadow__normal margin-bottom__big" data-aos="fade-up">
                            <div class="row">
                                <div class="col-xs-4 <?php if($addon_count % 2 != 0) echo 'last-md'; ?>">
                                    <div class="img-wrapper">
                                        <img src="<?php $img = get_sub_field('img'); echo $img['sizes']['medium']; ?>">
                                    </div>
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="font-size__normal"><?php the_sub_field('title'); ?></h4>
                                    <span class="price font-weight__bold text-color__text"><?php the_sub_field('price'); ?></span>
                                </div>
                            </div>
                        </article>
                    <?php $addon_count++; endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>

    <section id="home__cta" class="padding-top__section padding-bottom__mega--x background-color__grey">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-10 col-md-6">

                    <?php if (get_field('checkout_cta_title_img')): ?>

                        <img src="<?php $img = get_field('checkout_cta_title_img'); echo $img['sizes']['medium_large']; ?>">

                    <?php elseif(get_field('checkout_cta_title')): ?>
                        <h2 class="font-size__mega" data-aos="fade-up"><?php the_field('checkout_cta_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('checkout_cta_text')): ?>
                        <?php the_field('checkout_cta_text'); ?>
                    <?php endif; ?>

                    <?php if(get_field('checkout_cta_cta_link')): ?>
                        <a href="<?php the_field('checkout_cta_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('checkout_cta_cta_text'); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; get_footer();
