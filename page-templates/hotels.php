<?php
/**
 * Template Name: Hotels
 */
get_header();
while(have_posts()): the_post();
?>

    <div class="background-color__white padding__section text-color__titles section-intro">

        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-7">
                    <?php if(get_field('hotels_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__titles" data-aos="fade-up" data-aos-delay="200"><?php the_field('hotels_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('hotels_title')): ?>
                        <h1 class="font-size__mega text-color__titles"><?php the_field('hotels_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('hotels_text')): ?>
                        <?php the_field('hotels_text'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php
        $filter_location = $_GET['filter_location'] ? '&location.country=' . $_GET['filter_location'] : '';
        $offset = $_GET['offset'] ? '&_start=' . ($_GET['offset'] * 12) : '';
        $sortby = $_GET['sortby'] ? $_GET['sortby'] : 'recent';

        $rest_url = 'https://back.guruhotel.com/hotels?_limit=12&_sort=order&featured_hotel=1'.$offset.$filter_location;
        $results = file_get_contents($rest_url);
        $results = json_decode($results);
    ?>

    <div class="hotels-cards padding-top__small-section">
        <div class="container-fluid wrap">

            <div class="row margin-top__mega">
                <?php
                    $current_language = get_locale();
                    $i = 0;
                    foreach($results as $result):
                        $hotelDesc = array();

                        foreach ($result->langs as $lang) {
                            $hotelDesc[$lang->code] = $lang->description;
                        }
                ?>

                    <div class="col-xs-12 col-md-4">
                        <div class="card background-color__white border-radius__normal box-shadow__medium display__inline--block padding__mega--x" data-aos="fade-up">
                            <div class="img-wrapper">
                                <?php
                                    $imageID = $result->featured_image;
                                    if($imageID){
                                        $imageKey = array_search($imageID, array_column($result->images, 'id'));
                                        $imageUrl = $result->images[$imageKey]->formats->medium->url ? $result->images[$imageKey]->formats->medium->url : $result->images[$imageKey]->url;
                                        $imageUrl = $imageUrl ? $imageUrl : $result->photos[$imageKey]->formats->medium->url;
                                    }
                                    else{
                                        $imageUrl = $result->images[0]->formats->medium->url ? $result->images[0]->formats->medium->url : $result->images[0]->url;
                                        $imageUrl = $imageUrl ? $imageUrl : $result->photos[0]->formats->medium->url;
                                    }
                                ?>
                                <span class="location"><i class="fas fa-map-marker-alt"></i><?php echo $result->location->city.', '.$result->location->country; ?></span>
                                <?php if (!empty($imageUrl)): ?>
                                    <a href="#" data-toggle="modal" data-target="#modal-<?php echo $result->id; ?>">
                                         <img src="<?php echo $imageUrl; ?>" alt="<?php echo $result->name; ?> Photo" loading="lazy">
                                    </a>
                                <?php elseif(!empty($result->logo->url)): ?>
                                    <img src="<?php echo $result->logo->url; ?>" alt="<?php echo $result->name; ?> Logo" loading="lazy" class="hotel__logo">
                                <?php endif ?>
                            </div>
                            <div class="body-wrapper font-size__small--x">
                                <header>
                                    <a href="#" data-toggle="modal" data-target="#modal-<?php echo $result->id; ?>"><h2 class="font-size__normal"><?php echo $result->name; ?></h2></a>
                                    <div class="stars">
                                        <?php $stars = $result->stars; for($star = 1; $star<=$stars; $star++): ?>
                                            <i class="fas fa-star text-color__main"></i>
                                        <?php endfor; ?>
                                    </div>
                                </header>

                                <div class="hotel-desc">
                                    <?php if ($current_language == 'en_US' && $hotelDesc['en']): ?>
                                        <p class="margin-bottom__big"><?php echo substr($hotelDesc['en'], 0, strrpos(substr($hotelDesc['en'], 0, 180), ' ')); ?>...</p>
                                    <?php elseif($hotelDesc['es']): ?>
                                        <p class="margin-bottom__big"><?php echo substr($hotelDesc['es'], 0, strrpos(substr($hotelDesc['es'], 0, 180), ' ')); ?>...</p>
                                    <?php else: ?>
                                        <p class="margin-bottom__big"><?php echo substr($result->description, 0, strrpos(substr($result->description, 0, 180), ' ')); ?>...</p>
                                    <?php endif ?>
                                </div>

                                <div class="hotel-footer">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <a href="#"  data-toggle="modal" data-target="#modal-<?php echo $result->id; ?>" class="btn btn--primary border-radius__normal background-color__titles text-color__white display__inline--block font-size__small--x font-weight__normal"><i class="fas fa-info-circle margin-right__normal"></i><?php _e('See more', 'gh-apollo'); ?></a>
                                        </div>
                                        <div class="col-xs-6">
                                            <?php
                                                if (!empty($result->subdomain)){
                                                    $hotelLink = 'http://'.$result->subdomain;
                                                }
                                                else{
                                                    $parsed = parse_url($result->website);
                                                    if (empty($parsed['scheme'])) {
                                                        $hotelLink = 'http://' . ltrim($result->website, '/');
                                                    }
                                                }
                                             ?>
                                            <?php if (!empty($result->subdomain)): ?>
                                                <a href="<?php echo $hotelLink; ?>" target="_blank" class="btn btn--primary border-radius__normal background-color__titles text-color__white display__inline--block font-size__small--x font-weight__normal"><i class="fas fa-globe margin-right__normal"></i><?php _e('Visit website', 'gh-apollo'); ?></a>
                                            <?php else: ?>
                                                <a href="<?php echo $hotelLink; ?>" target="_blank" class="btn btn--primary border-radius__normal background-color__titles text-color__white display__inline--block font-size__small--x font-weight__normal"><i class="fas fa-globe margin-right__normal"></i><?php _e('Visit website', 'gh-apollo'); ?></a>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php $i++; endforeach; ?>
            </div>

            <?php
                //Count hotels
                $offset = $_GET['offset'];
                $count_url = 'https://back.guruhotel.com/hotels/count?_where[featured_hotel]=1';
                $hotels_count = file_get_contents($count_url);
                $posts_per_page = 12;

                if($hotels_count>$posts_per_page):

                    $pages_count = ceil($hotels_count / $posts_per_page);

                    if ($pages_count <= 10) {
                        $start = 1;
                        $end   = $pages_count;
                    } else {
                        $start = max(1, ($offset - 4));
                        $end   = min($pages_count, ($offset + 5));

                        if ($start === 1) {
                            $end = 10;
                        } elseif ($end === $pages_count) {
                            $start = ($pages_count - 9);
                        }
                    }
            ?>
                <div class="hotels-pagination">
                    <ul>
                        <?php if (!empty($offset) && $offset > 0 && $pages_count >= 6 ): ?>
                            <li><a href="<?php the_permalink(); ?>?featured_hotel=1&offset=0"><i class="fas fa-angle-double-left"></i></a></li>
                        <?php endif ?>

                        <?php if (!empty($offset) && $offset > 0 ): ?>
                            <li><a href="<?php the_permalink(); ?>?featured_hotel=1&offset=<?php echo $offset - 1; ?>"><i class="fas fa-angle-left"></i></a></li>
                        <?php endif ?>

                        <?php for($page = $start; $page<=$end; $page++): ?>
                            <li <?php if($offset + 1 == $page || ($page == 1 && empty($offset))) echo 'class="current"'; ?>><a href="<?php the_permalink(); ?>?featured_hotel=1&offset=<?php echo $page - 1; ?>"><?php echo $page; ?></a></li>
                        <?php endfor; ?>

                        <?php if ($offset + 1 < $pages_count ): ?>
                            <li><a href="<?php the_permalink(); ?>?featured_hotel=1&offset=<?php echo $offset + 1; ?>"><i class="fas fa-angle-right"></i></a></li>
                        <?php endif ?>

                        <?php if ($offset + 1 < $pages_count && $pages_count >= 6 ): ?>
                            <li><a href="<?php the_permalink(); ?>?featured_hotel=1&offset=<?php echo $pages_count - 1; ?>"><i class="fas fa-angle-double-right"></i></a></li>
                        <?php endif ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php
        $current_language = get_locale();
        $i = 0;
        foreach($results as $result):
            $hotelDesc = array();

            foreach ($result->langs as $lang) {
                $hotelDesc[$lang->code] = $lang->description;
            }
    ?>
        <div class="modal-hotel fade" id="modal-<?php echo $result->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-fade"></div>
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-target="#modal-<?php echo $result->id; ?>">
                  <i class="fas fa-times"></i>
                </button>

                <div class="modal-hotel__header">
                    <?php
                        if($imageID){
                            $imageKey = array_search($imageID, array_column($result->images, 'id'));
                            $imageUrl = $result->images[$imageKey]->formats->medium->url ? $result->images[$imageKey]->formats->medium->url : $result->images[$imageKey]->url;
                            $imageUrl = $imageUrl ? $imageUrl : $result->photos[$imageKey]->formats->medium->url;
                        }
                        else{
                            $imageUrl = $result->images[0]->formats->medium->url ? $result->images[0]->formats->medium->url : $result->images[0]->url;
                            $imageUrl = $imageUrl ? $imageUrl : $result->photos[0]->formats->medium->url;
                        }
                    ?>
                    <img src="<?php echo $imageUrl; ?>" alt="<?php echo $result->name; ?> Photo" loading="lazy" class="modal-hotel__header-bg">

                    <div class="modal-hotel__header-content">
                        <div class="stars">
                            <?php $stars = $result->stars; for($star = 1; $star<=$stars; $star++): ?>
                                <i class="fas fa-star text-color__white"></i>
                            <?php endfor; ?>
                        </div>
                        <h2 class="font-size__big text-color__white"><?php echo $result->name; ?></h2>

                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                    if (!empty($result->subdomain)){
                                        $hotelLink = 'http://'.$result->subdomain;
                                    }
                                    else{
                                        $parsed = parse_url($result->website);
                                        if (empty($parsed['scheme'])) {
                                            $hotelLink = 'http://' . ltrim($result->website, '/');
                                        }
                                    }
                                 ?>
                                <?php if (!empty($result->subdomain)): ?>
                                    <a href="<?php echo $hotelLink; ?>" target="_blank" class="font-size__small--x"><i class="fas fa-globe margin-right__normal"></i><?php echo $result->subdomain; ?></a>
                                <?php else: ?>
                                    <a href="<?php echo $hotelLink; ?>" target="_blank" class="font-size__small--x"><i class="fas fa-globe margin-right__normal"></i><?php echo $result->website; ?></a>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>

                <nav class="nav-tabs">
                    <ul>
                        <li><a href="#tab--desc" class="current"><?php _e('Description', 'gh-apollo'); ?></a></li>
                        <?php if(!empty($result->amenities)): ?>
                            <li><a href="#tab--amenities"><?php _e('Amenities', 'gh-apollo'); ?></a></li>
                        <?php endif; ?>
                        <li><a href="#tab--gallery"><?php _e('Gallery', 'gh-apollo'); ?></a></li>
                    </ul>
                </nav>

                <div class="tab-content__wrapper">
                    <div class="tab tab--desc tab--active" id="tab--desc">
                        <span class="location"><i class="fas fa-map-marker-alt"></i><?php echo $result->location->city.', '.$result->location->country; ?></span>
                        <?php if ($current_language == 'en_US' && $hotelDesc['en']): ?>
                            <p class="margin-bottom__big"><?php echo $hotelDesc['en']; ?></p>
                        <?php elseif($hotelDesc['es']): ?>
                             <p class="margin-bottom__big"><?php echo $hotelDesc['es']; ?></p>
                        <?php else: ?>
                            <p class="margin-bottom__big"><?php echo $hotelDesc['es']; ?></p>
                        <?php endif ?>
                    </div>

                    <?php if(!empty($result->amenities)): ?>
                        <div class="tab tab--amenities" id="tab--amenities">

                            <ul class="start-xs">
                                <?php foreach ($result->amenities as $amenity): ?>
                                    <li><?php echo $amenity->name; ?></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <div class="tab tab--gallery" id="tab--gallery">
                        <div class="hotel-modal__gallery-slider">
                            <?php foreach ($result->images as $image): ?>
                                <?php
                                    $imageUrl = $image->formats->medium->url ? $image->formats->medium->url : $image->url;
                                ?>
                                <img src="<?php echo $imageUrl; ?>" loading="lazy">
                            <?php endforeach ?>
                        </div>
                    </div>

                </div>
              </div>
            </div>
          </div>
        </div>
    <?php endforeach; ?>

    <section class="padding-top__small-section padding-bottom__mega--x background-color__grey">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-10 col-md-6">

                    <?php if(get_field('hotels_cta_title')): ?>
                        <h2 class="font-size__mega"><?php the_field('hotels_cta_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('hotels_cta_text')): ?>
                        <?php the_field('hotels_cta_text'); ?>
                    <?php endif; ?>

                    <?php if(get_field('hotels_cta_cta_link')): ?>
                        <a href="<?php the_field('cta_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('hotels_cta_cta_text'); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; get_footer();
