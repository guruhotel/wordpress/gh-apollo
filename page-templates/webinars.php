<?php
/**
 * Template Name: Product - Webinars
 */
get_header();
while(have_posts()): the_post();
?>
    <div class="background-color__main padding__section text-color__white section-intro">
                <?php
                    $first = true;
                    if(have_rows('upcoming_webinars')) : while(have_rows('upcoming_webinars')): the_row();
                ?>
                    <div>
                        <div class="container-fluid wrap">
                            <div class="row middle-xs">
                                <div class="col-sm-4 col-md-5 image-column last-md">
                                    <a href="<?php the_sub_field('link'); ?>" class="webinar-thumb">
                                        <img src="<?php $img = get_sub_field('img'); echo $img['sizes']['medium_large']; ?>" class="border-radius__normal box-shadow__normal">
                                    </a>

                                    <div class="col-md-2 author-column first-md hidden__lg hidden__md hidden__xs">
                                        <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                            <article class="webinar__author margin-bottom__medium">
                                                <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                                <div>
                                                    <span class="name font-size__small--x"><?php _e('By', 'gh-apollo'); ?> <strong><?php the_sub_field('name'); ?></strong></span>
                                                    <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                                </div>
                                            </article>
                                        <?php endwhile; endif; ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-8 col-md-5 content-column">
                                    <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white"><?php _e('Upcoming webinar', 'gh-apollo'); ?></h4>

                                    <h1 class="font-size__mega text-color__white"><?php the_sub_field('title'); ?></h1>

                                    <div class="upcoming-webinar__data">
                                        <i class="fas fa-calendar-alt"></i>
                                        <span>
                                            <?php
                                                $date_string = get_sub_field('date');
                                                $date = DateTime::createFromFormat('d/m/Y g:i a', $date_string);
                                                echo $date->format('F j, Y \| g:i a');
                                            ?>
                                        </span>
                                        <span class="margin-left__normal margin-right__normal info__separator">|</span>
                                        <i class="fa fa-clock"></i>
                                        <span><?php the_sub_field('duration'); ?></span>
                                    </div>

                                    <div><?php the_sub_field('text'); ?></div>

                                    <footer>
                                        <a href="<?php the_sub_field('link'); ?>" target="_blank" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php _e('Register', 'gh-apollo'); ?><i class="fas fa-long-arrow-alt-right margin-left__normal" aria-hidden="true"></i></a>

                                        <a href="#upcoming-webinars" class="btn webinars__order-by-btn btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><i class="fas fa-calendar-alt margin-right__normal margin-left__normal"></i><?php _e('Upcoming webinars', 'gh-apollo'); ?></a>
                                    </footer>
                                </div>

                                <div class="col-md-2 author-column first-md hidden__sm">
                                    <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                        <article class="webinar__author margin-bottom__medium center-xs">
                                            <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                            <div>
                                                <span class="name font-size__small--x"><?php _e('By', 'gh-apollo'); ?> <strong><?php the_sub_field('name'); ?></strong></span>
                                                <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                            </div>
                                        </article>
                                    <?php endwhile; endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                    endwhile; endif;
                    $args = (array(
                       'post_type' => 'guru_webinar',
                       'posts_per_page' => 4,
                        'meta_query' => array(
                            array(
                                'key'   => 'webinar_featured',
                                'value' => '1',
                            )
                        )
                    ) );
                    $query = new WP_Query($args);
                    if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
                    $featured_webinar = get_the_ID();
                ?>

                    <div>
                        <div class="container-fluid wrap">
                            <div class="row middle-xs">
                                <div class="col-sm-4 col-md-5 image-column last-md">
                                    <a href="<?php the_permalink(); ?>" class="webinar-thumb">
                                        <?php the_post_thumbnail('medium_large', array('class' => 'border-radius__normal box-shadow__normal')); ?>
                                        <i class="fa fa-play box-shadow__normal"></i>
                                    </a>

                                        <div class="col-md-2 author-column first-md hidden__lg hidden__md hidden__xs">
                                        <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                            <article class="webinar__author">
                                                <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                                <div>
                                                    <span class="name font-size__small--x">Por <strong><?php the_sub_field('name'); ?></strong></span>
                                                    <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                                </div>
                                            </article>
                                        <?php endwhile; endif; ?>
                                    </div>
                                </div>

                                <div class="col-sm-8 col-md-5 content-column">
                                    <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white" data-aos="fade-up" data-aos-delay="200"><?php _e('Featured webinar', 'guruhotel'); ?></h4>

                                     <h1 class="font-size__mega text-color__white"><?php the_title(); ?></h1>

                                    <?php the_excerpt(); ?>

                                    <a href="<?php the_permalink(); ?>" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__mega--x font-size__small--x hidden__xs">Ver webinar</a>
                                </div>

                                <div class="col-md-2 author-column first-md hidden__sm first-md center-xs">
                                    <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                        <article class="webinar__author margin-bottom__medium">
                                            <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                            <div>
                                                <span class="name font-size__small--x"><?php _e('By', 'gh-apollo'); ?> <strong><?php the_sub_field('name'); ?></strong></span>
                                                <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                            </div>
                                        </article>
                                    <?php endwhile; endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>

    <?php if(have_rows('upcoming_webinars')): ?>
        <div class="container-fluid wrap end-xs padding-top__small-section">
            <nav class="webinars__order-by">
                <ul>
                    <li class="current">
                        <a href="#recent-webinars" class="webinars__order-by-btn"><?php _e('Recent', 'gh-apollo'); ?></a>
                    </li>
                    <li>
                        <a href="#upcoming-webinars" class="webinars__order-by-btn"><?php _e('Upcoming', 'gh-apollo'); ?></a>
                    </li>
                </ul>
            </nav>
        </div>
    <?php endif ?>

    <div class="webinar-list padding__small-section webinar-list__recent">
        <div class="webinar-list__webinars active" id="recent-webinars">
            <div class="container-fluid wrap">
                <div class="row">
                       <?php
                           $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                           if(have_rows('upcoming_webinars')) $featured_webinar = ''; // If there are slides on upcoming webinars, show again the featured webinar on the listing
                           $args = (array(
                               'post_type' => 'guru_webinar',
                               'posts_per_page' => 9,
                               'paged' => $paged,
                               'post__not_in'=> array($featured_webinar)
                           ) );
                           $query = new WP_Query($args);
                           if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
                       ?>
                        <div class="col-sm-6 col-md-4 margin-bottom__big">
                            <div class="webinar-item card background-color__white border-radius__normal box-shadow__medium" data-aos="fade-up">

                                <div class="webinar-item__thumb">
                                    <a href="<?php the_permalink( ); ?>">
                                        <?php the_post_thumbnail('medium_large'); ?>
                                    </a>
                                </div>

                                <div class="webinar-item__content padding__mega">
                                    <div class="row margin-bottom__medium">
                                        <div class="col-xs-8">
                                            <time class="meta font-size__small--x font-weight__normal text-color__text"><i class="far fa-calendar text-color__main" aria-hidden="true"></i>
                                            <time><?php echo get_the_date(); ?></time></time>
                                        </div>
                                        <div class="col-xs-4 end-xs font-size__small--x webinar__duration">
                                            <?php if(get_field('webinar_duration')): ?>
                                                <i class="fa fa-clock text-color__main"></i>
                                                <?php the_field('webinar_duration'); ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <a href="<?php the_permalink(); ?>"><h2 class="article-title font-size__medium margin-bottom__normal"><?php the_title(); ?></h2></a>

                                    <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                        <article class="webinar__author margin-bottom__normal">
                                            <div class="row middle-xs">
                                                <div class="col-xs-3">
                                                    <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                                </div>
                                                <div class="col-xs-9">
                                                     <span class="name font-size__small--x">Por <strong><?php the_sub_field('name'); ?></strong></span>
                                                     <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                                </div>
                                            </div>
                                        </article>
                                    <?php endwhile; endif; ?>

                                    <a href="<?php the_permalink(); ?>" class="btn__read font-weight__normal font-size__small--x text-color__main"><?php _e('Ver webinar', 'apollo'); ?><i class="fas fa-long-arrow-alt-right margin-left__normal" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                            </article>
                        </div>
                       <?php endwhile; endif; ?>

                        <div class="pagination">
                        <?php
                          $big = 999999999; // need an unlikely integer

                          echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'total' => $query->max_num_pages,
                            'prev_text' => '<',
                            'next_text' => '>'
                          ) );
                        ?>
                        </div>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>

         <div class="webinar-list__webinars" id="upcoming-webinars">
            <?php
                if(have_rows('upcoming_webinars')) :
            ?>

                    <div class="container-fluid wrap">
                        <div class="row">
                               <?php
                                   while(have_rows('upcoming_webinars')): the_row();
                               ?>
                                <div class="col-sm-6 col-md-4 margin-bottom__big">
                                    <div class="webinar-item card background-color__white border-radius__normal box-shadow__medium" data-aos="fade-up">

                                        <div class="webinar-item__thumb">
                                            <a href="<?php the_sub_field('link'); ?>" class="">
                                                <img src="<?php $img = get_sub_field('img'); echo $img['sizes']['medium_large']; ?>" class="border-radius__normal box-shadow__normal">
                                            </a>
                                        </div>

                                        <div class="webinar-item__content padding__mega">
                                            <div class="row margin-bottom__medium">
                                                <div class="col-xs-8">
                                                    <time class="meta font-size__small--x font-weight__normal text-color__text"><i class="far fa-calendar text-color__main" aria-hidden="true"></i>
                                                    <time><?php the_sub_field('date'); ?></time></time>
                                                </div>
                                                <div class="col-xs-4 end-xs font-size__small--x ">
                                                    <?php if(get_sub_field('duration')): ?>
                                                        <i class="fa fa-clock text-color__main"></i>
                                                        <?php the_sub_field('duration'); ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>

                                            <a href="<?php the_sub_field('link'); ?>" target="_blank"><h2 class="article-title font-size__medium margin-bottom__normal"><?php the_sub_field('title'); ?></h2></a>

                                            <?php if(have_rows('webinar_authors')) : while(have_rows('webinar_authors')): the_row(); ?>
                                                <article class="webinar__author margin-bottom__normal">
                                                    <div class="row middle-xs">
                                                        <div class="col-xs-3">
                                                            <img src="<?php $img = get_sub_field('photo'); echo $img['sizes']['thumbnail']; ?>">
                                                        </div>
                                                        <div class="col-xs-9">
                                                             <span class="name font-size__small--x"><?php _e('By', 'gh-apollo'); ?> <strong><?php the_sub_field('name'); ?></strong></span>
                                                             <span class="font-size__small--x"><?php the_sub_field('role'); ?></span>
                                                        </div>
                                                    </div>
                                                </article>
                                            <?php endwhile; endif; ?>

                                            <a href="<?php the_sub_field('link'); ?>" target="_blank" class="btn__read font-weight__normal font-size__small--x text-color__main"><?php _e('Register', 'gh-apollo'); ?><i class="fas fa-long-arrow-alt-right margin-left__normal" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>

                                    </article>
                                </div>
                            <?php endwhile;?>
                        </div>
                    </div>
            <?php else: ?>
                <h4><?php _e('There aren\'t upcoming webinars.', 'understrap'); ?></h4>
            <?php endif; ?>
        </div>
    </div>

<?php endwhile; get_footer();
