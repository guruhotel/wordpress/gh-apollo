<?php
/**
 * Template Name: Advisory
 */
get_header();
while(have_posts()): the_post();
?>

    <div class="background-color__white padding__section text-color__titles section-intro">

        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-7">
                    <?php if(get_field('advisory_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__titles" data-aos="fade-up" data-aos-delay="200"><?php the_field('advisory_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('advisory_title')): ?>
                        <h1 class="font-size__mega text-color__titles" data-aos="fade-up" data-aos-delay="400"><?php the_field('advisory_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('advisory_text')): ?>
                        <div class="margin-bottom__mega--x" data-aos="fade-up" data-aos-delay="600"><?php the_field('advisory_text'); ?></div>
                    <?php endif; ?>

                    <?php if(get_field('advisory_intro_cta_link')): ?>
                           <a href="<?php the_field('advisory_intro_cta_link'); ?>" data-aos="fade-up" data-aos-delay="800" class="btn btn--primary border-radius__normal background-color__main text-color__white padding__medium--x margin-top__mega--x font-size__small--x"><?php the_field('advisory_intro_cta_text'); ?></a>
                       <?php endif; ?>
                </div>
            </div>
        </div>
    </div>



    <?php if(have_rows('advisory_team_members')) : ?>
        <section class="background-color__titles padding__section">
            <div class="container-fluid wrap">
                <div class="row center-xs">
                    <div class="col-xs-11 col-md-6 text-color__white start-xs">
                         <?php if(get_field('advisory_team_subline')): ?>
                           <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white" data-aos="fade-up" data-aos-delay="200"><?php the_field('advisory_team_subline'); ?></h4>
                        <?php endif; ?>

                        <?php if(get_field('advisory_team_title')): ?>
                            <h1 class="font-size__mega text-color__white"><?php the_field('advisory_team_title'); ?></h1>
                        <?php endif; ?>

                        <?php if(get_field('advisory_team_text')): ?>
                            <div class="margin-bottom__mega--x"><?php the_field('advisory_team_text'); ?></div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row center-xs">
                    <div class="col-xs-11 col-md-10">
                        <div class="sliders-wrapper">
                            <div class="members__slider start-xs">
                            <?php if(have_rows('advisory_team_members')) : while(have_rows('advisory_team_members')): the_row(); ?>
                                <article class="item members__slider-member">
                                    <div class="item-wrapper">
                                        <div class="member-img">
                                            <img src="<?php $img = get_sub_field('img'); echo $img['sizes']['medium_large']; ?>">
                                        </div>

                                    <div class="member-card background-color__white">
                                            <header class="padding-bottom__medium">
                                                <h4><?php the_sub_field('name'); ?></h4>

                                                <?php if(get_sub_field('role')): ?>
                                                    <span class="role text-color__titles"><?php the_sub_field('role'); ?></span>
                                                <?php endif; ?>
                                            </header>

                                            <?php if(get_sub_field('expertise')): ?>
                                                <p><?php _e('Experience in', 'gh-apollo'); ?> <span class="text-color__main font-weight__bold"><?php the_sub_field('expertise'); ?></span></p>
                                            <?php endif; ?>

                                            <?php if(get_sub_field('bio')): ?>
                                                <?php the_sub_field('bio'); ?>
                                            <?php endif; ?>

                                            <?php if(get_sub_field('link')): ?>
                                                <div class="end-xs margin-top__medium">
                                                    <a href="<?php the_sub_field('link'); ?>" class="btn btn--primary border-radius__normal background-color__titles text-color__white display__inline--block font-size__small--x font-weight__normal margin-top__medium"><i class="fas fa-comments text-color__main margin-right__normal"></i><?php _e('Let\'s chat!', 'gh-apollo'); ?></a>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                </article>
                            <?php endwhile; endif; ?>
                        </div>

                        <div class="members__img-slider start-xs">
                            <?php if(have_rows('advisory_team_members')) : while(have_rows('advisory_team_members')): the_row(); ?>
                                <article class="item members__img-slider-member">
                                    <div class="item-wrapper">
                                        <div class="member-img">
                                            <img src="<?php $img = get_sub_field('img'); echo $img['sizes']['medium_large']; ?>">
                                        </div>
                                    </div>
                                </article>
                            <?php endwhile; endif; ?>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <div class="services__cards padding__small-section background-color__white">
        <div class="container-fluid wrap center-xs">

            <?php if(get_field('advisory_services_title')): ?>
                <h2 class="services__cards-title"><?php the_field('advisory_services_title'); ?></h2>
            <?php endif; ?>

            <div class="row start-xs">
                <?php
                  $args = array(
                      'post_type'      => 'guru_service',
                      'posts_per_page' => -1,
                  );
                  $query = new WP_Query( $args );
                  if($query->have_posts()): while($query->have_posts()) : $query->the_post();
                ?>
                    <div class="col-xs-11 col-md-4">
                        <div class="card background-color__white border-radius__normal box-shadow__medium display__inline--block padding__mega--x">
                            <div class="img-wrapper">
                                <?php
                                    if (get_field('service_embed')):
                                        $current_language = get_locale();
                                        if( $current_language == 'es_MX' && get_field('service_embed_es') ){
                                            the_field('service_embed_es');
                                        }
                                        else{
                                            the_field('service_embed');
                                        }
                                    else:
                                ?>
                                    <a href="<?php the_field('service_video'); ?>" data-lity>
                                        <i class="fa fa-play box-shadow__normal"></i>
                                        <?php the_post_thumbnail('medium_large'); ?>
                                    </a>
                                <?php endif ?>
                            </div>
                            <div class="body-wrapper font-size__small--x">
                                <header>
                                    <a href="<?php the_field('service_link'); ?>" target="_blank"><h2 class="font-size__normal"><?php the_title(); ?></h2></a>
                                </header>

                                <div class="service-desc">
                                    <?php the_excerpt(); ?>
                                </div>

                                <div class="service-footer">
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <a href="<?php the_field('service_link'); ?>" target="_blank" class="btn btn--primary border-radius__normal background-color__titles text-color__white display__inline--block font-size__small--x font-weight__normal"><i class="fas fa-comments text-color__main margin-right__normal"></i><?php _e('Chat with an expert', 'gh-apollo'); ?></a>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="service-price">
                                                <h6><?php _e('Starting at', 'gh-apollo'); ?></h6>
                                                <span class="display__block font-weight__bold text-color__titles font__secondary"><?php the_field('service_price'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>

    <?php if(have_rows('advisory_features')) : ?>
        <section class="background-color__main padding__section text-color__white section-features">
            <div class="container-fluid wrap">
                <div class="row center-xs">

                    <div class="col-xs-11 col-md-3 end-xs">
                        <?php if(get_field('advisory_stats_title')): ?>
                            <h2 class="text-color__white"><?php the_field('advisory_stats_title'); ?></h2>
                        <?php endif; ?>
                        <?php if(get_field('advisory_stats_text')): ?>
                            <?php the_field('advisory_stats_text'); ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-11 col-md-4 start-xs">
                        <ul>
                            <?php while(have_rows('advisory_features')): the_row(); ?>
                                <li>
                                    <i class="<?php the_sub_field('icon'); ?>"></i>
                                    <?php the_sub_field('title'); ?>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
    <script type="text/javascript" async src="https://play.vidyard.com/embed/v4.js"></script>
<?php endwhile; get_footer();
