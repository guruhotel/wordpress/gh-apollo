<?php
/**
 * Template Name: Credits
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    <section class="padding-top__small-section background-color__white">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-11">
                        <?php if(get_field('intro_preimage')): ?>
                            <img src="<?php $img = get_field('intro_preimage'); echo $img['sizes']['medium_large']; ?>" class="margin-bottom__mega--x margin-top__mega--x banner-preimg">
                        <?php endif; ?>
                </div>
                <div class="col-xs-11 col-md-8">
                    <?php if (get_field('intro_pretitle')): ?>
                        <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase" <?php if(is_front_page()) echo 'data-aos="fade-up" data-aos-delay="200"'; ?>><?php the_field('intro_pretitle'); ?></h4>
                    <?php endif ?>

                    <?php if(get_field('intro_title_first_part')): ?>
                       <h1 class="banner-title font-size__mega text-color__titles" <?php if(is_front_page()) echo 'data-aos="fade-up" data-aos-delay="400"'; ?>><?php the_field('intro_title_first_part'); ?> <span class="text-color__yellow display__inline-block"><?php the_field('intro_title_second_part'); ?></span></h1>
                    <?php endif; ?>

                    <?php if(get_field('intro_text')): ?>
                       <div class="banner-desc text-color__titles margin-top__mega--x" <?php if(is_front_page()) echo 'data-aos="fade-up" data-aos-delay="600"'; ?>><?php the_field('intro_text'); ?></div>
                   <?php endif; ?>

                   <?php if(get_field('intro_cta_link')): ?>
                        <a href="<?php the_field('intro_cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__normal background-color__yellow text-color__titles padding__medium--x display__inline--block margin-top__mega font-size__small--x banner-button" <?php if(is_front_page()) echo ' data-aos="fade-up" data-aos-delay="800"'; ?>>
                            <?php the_field('intro_cta_text'); ?>
                        </a>
                   <?php endif; ?>
                </div>

                <div class="col-xs-11 col-md-8">
                    <?php if(get_field('intro_extra_text')): ?>
                        <div class="banner-extra-text margin-top__mega--x text-color__titles border-radius__medium--x " <?php if(is_front_page()) echo 'data-aos="fade-up" data-aos-delay="600"'; ?>>
                           <?php the_field('intro_extra_text'); ?>
                       </div>
                   <?php endif; ?>
                </div>

                <?php if(have_rows('intro_features')) : ?>
                    <div class="col-xs-11 col-md-11">
                        <div class="banner-features margin-top__mega--x">
                            <div class="row">
                                <?php
                                    $features_count = 1;
                                    while(have_rows('intro_features')): the_row();
                                ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="card start-xs border-radius__small--x background-color__white margin-top__mega--x" data-aos="fade-up" data-aos-delay="<?php echo $features_count*50; ?>">
                                            <div class="row middle-xs">
                                                <div class="col-xs-3 col-sm-2">
                                                    <span class="background-color__yellow text-color__titles">
                                                        <i class="<?php the_sub_field('icon'); ?> font-size__normal"></i>
                                                    </span>
                                                </div>
                                                <div class="col-xs-9 col-sm-10">
                                                    <h4 class="text-color__titles"><?php the_sub_field('text'); ?></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $features_count++; endwhile; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <section class="background-color__black text-color__white padding__section credits__form-section">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-8 col-lg-6">
                    <?php if (get_field('banner_pretitle')): ?>
                        <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white"><?php the_field('banner_pretitle'); ?></h4>
                    <?php endif ?>

                    <?php if(get_field('banner_title')): ?>
                        <h2 class="text-color__white margin-top__normal"><?php the_field('banner_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('banner_text')): ?>
                        <div class="text-color__white"><?php the_field('banner_text'); ?></div>
                    <?php endif; ?>
                </div>

                <div class="col-xs-11 col-md-10">
                    <div class="background-color__white credits__form-section-steps border-radius__medium--x text-color__titles" id="forms">
                        <?php echo do_shortcode( get_field('banner_form'), false ); ?>
                    </div>
                </div>

                <?php if(get_field('banner_alert')): ?>
                    <div class="col-xs-11 col-md-8 margin-top__mega--x banner_alert">
                        <?php the_field('banner_alert'); ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </section>

<?php endwhile; get_footer();
