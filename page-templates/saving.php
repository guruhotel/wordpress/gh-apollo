<?php
/**
 * Template Name: SaveIndieHotels
 */
get_header();
while(have_posts()): the_post();
?>

<section id="saving">
    <div class="saving-hero background-color__utilitary padding-top__mega padding-bottom__mega">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-9">
                    <?php if(get_field('saving_intro_title_first_part')): ?>
                        <h1 class="hero-title font-weight__normal margin-bottom__big"><div data-aos="fade-up" data-aos-duration="1s"><?php the_field('saving_intro_title_first_part'); ?></div> <strong data-aos-duration="1s" class="display__block text-color__main" data-aos-delay="0.2s" data-aos="fade-up"><?php the_field('saving_intro_title_second_part'); ?></strong></h1>
                    <?php endif; ?>

                    <?php if(get_field('saving_intro_text')): ?>
                       <div class="hero-desc font-size__medium text-color__titles"><?php the_field('saving_intro_text'); ?></div>
                    <?php endif; ?>

                    <?php if(get_field('saving_intro_button_link')): ?>
                       <a href="<?php the_field('saving_intro_button_link'); ?>" class="btn hero-btn btn__size--medium font-weight__normal background-color__main display__inline--block margin-top__mega border-radius__mega--x text-color__white" data-aos="zoom-in"  data-aos-duration="1s" data-aos-delay="2s"><?php the_field('saving_intro_button_text'); ?> <i class="<?php the_field('saving_intro_button_icon'); ?> margin-left__small alpha-color"></i></a>
                    <?php endif; ?>

                    <img class="image" data-aos="fade-up" data-aos-duration="2s" data-aos-delay="0.6s" src="<?php bloginfo('template_directory') ?>/src/images/saveindiehotels.svg" alt="Save Indie Hotels" />
                    <div class="image-bg" style="background-image: url('<?php bloginfo('template_directory') ?>/src/images/hotels-bg.svg')"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid wrap">

        <?php if (have_rows('saving_steps')): ?>
            <div class="saving-steps padding__small-section row center-xs">
                <?php $step = 1; while(have_rows('saving_steps')): the_row(); ?>
                    <div class="col-xs-10 start-xs col-sm-9 col-md-4">
                        <div class="item" data-aos="fade-up" data-aos-delay="0.2s">
                            <span class="saving-steps__number"><?php echo $step; ?></span>
                            <h4 class="saving-steps__title"><?php the_sub_field('title'); ?></h4>
                            <div class="saving-steps__desc"><?php the_sub_field('text'); ?></div>
                        </div>
                    </div>
                <?php $step++; endwhile; ?>
            </div>
        <?php endif ?>

        <?php if(get_field('saving_problem_text')): ?>
            <div class="saving-problem">
                <div class="row center-xs">
                    <?php if(get_field('saving_problem_title')): ?>
                        <div class="col-xs-12">
                            <h2 class="margin-bottom__mega"><?php the_field('saving_problem_title'); ?></h2>
                        </div>
                    <?php endif; ?>
                    <div class="col-xs-11 start-xs font-size__medium col-md-9 text-color__titles margin-bottom__mega">
                       <?php the_field('saving_problem_text'); ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if(get_field('saving_donations_text')): ?>
            <div class="saving-covid row center-xs" data-aos="fade-up">
                <div class="col-xs-12 col-md-9">
                    <div class="item row middle-xs center-xs around-md">
                        <div class="saving-covid__image col-xs-12 col-sm-2">
                            <img src="<?php bloginfo('template_directory') ?>/src/images/mask-user.png" />
                        </div>
                        <div class="saving-covid__text col-xs-11 col-sm-9 start-xs">
                            <?php the_field('saving_donations_text'); ?>

                            <a href="<?php the_field('saving_donations_btn_link'); ?>" target="_blank" class="highlight utilitary-border font-weight__normal" rel="nofollow"><?php the_field('saving_donations_btn_text'); ?> <i class="fas fa-arrow-right font-size__small"></i></i></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>


        <div class="col-xs-12 margin-top__mega">
            <h2 class="margin-bottom__medium center-xs">Hoteles independientes</h2>
            <div class="saving-hotels">
                <div class="selector">
                    <div class="center-xs text-color__titles margin-bottom__small text-transform__uppercase font-size__small letter-spacing__medium"><i class="fas fa-map-marker-alt"></i> Filtrar por ubicación:</div>
                    <ul data-tabs class="saving-hotels__tabs center-xs">
                        <?php
                        $terms = get_terms([
                            'taxonomy' => 'saving-tax',
                            'hide_empty' => true,
                            'orderby'    => 'name',
                            'order'      => 'ASC',
                        ]);
                        if( $terms ) :
                            $i = 0;
                            foreach ( $terms as $item ) { ; $i++; ?>
                                <li class="option" values="<?php echo $i ?>"><a <?php if($i==1) { echo 'data-tabby-default'; }; ?> href="#tab-<?php echo $item->slug; ?>"><?php echo $item->name; ?> <i class="fas fa-chevron-down down-icon"></i></a></li>
                            <?php }
                        endif; ?>
                    </ul>
                </div>
                <?php
                $terms = get_terms([
                    'taxonomy' => 'saving-tax',
                    'hide_empty' => true,
                    'orderby'    => 'name',
                    'order'      => 'ASC',
                ]);
                if( $terms ) :
                    $i = 0;
                    foreach ( $terms as $item ) { $i++; ?>
                        <?php
                        $wp_query = new WP_Query(array(
                            'post_type' => 'saving_hotels',
                            'tax_query' => array(
                                array (
                                    'taxonomy' => 'saving-tax',
                                    'field' => 'slug',
                                    'terms' => $item->slug,
                                )
                            ),
                        )); ?>
                        <div id="tab-<?php echo $item->slug; ?>">
                            <div class="saving-hotels-row">
                                <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                                    <div class="saving-hotel">
                                        <a href="<?php echo get_post_meta(get_the_ID(),'saving_url',true) ?>" target="_blank" rel="nofollow">
                                            <div class="card card__size--big border-color__grey--regent box-shado__mega border-radius__normal">
                                                <div class="saving-hotel__thumb" style="background-image: url('<?php the_post_thumbnail_url('post-thumb'); ?>');">
                                                    <img src="<?php the_post_thumbnail_url('post-thumb'); ?>" />
                                                </div>
                                                <div class="saving-hotel__caption">
                                                    <h2 class="saving-hotel__caption--title"><?php the_title(); ?></h2>
                                                    <?php
                                                        $get_price = get_post_meta(get_the_ID(),'saving_price',true);
                                                        if($get_price == '150') {
                                                            echo '<span class="text-color__pink display__block">$150 gift card</span>';
                                                        } else if($get_price == '100') {
                                                            echo '<span class="text-color__pink display__block">$100 gift card</span>';
                                                        } else {
                                                            echo '<span class="text-color__pink display__block">$50 gift card</span>';
                                                        }
                                                    ?>
                                                    <span class="font-size__small--x">Por 2 noches / 2 personas</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                <?php endwhile; wp_reset_query(); ?>
                            </div>
                        </div>
                    <?php }
                endif; ?>
            </div>
        </div>
    </div>

    <?php if(get_field('saving_cta_btn_link')): ?>
        <div class="saving-cta background-color__main padding__small-section">
                <div class="container-fluid wrap">
                    <div class="row center-xs">
                        <div class="col-xs-10 col-sm-9">
                            <h3 class="saving-cta__title text-color__white line-height__big"><?php the_field('saving_cta_text'); ?></h3>
                            <a href="<?php the_field('saving_cta_btn_link'); ?>" class="btn saving-cta__btn"><?php the_field('saving_cta_btn_text'); ?> <i class="<?php the_field('saving_cta_btn_icon'); ?>"></i></a>
                        </div>
                    </div>
                </div>
            </div>
    <?php endif; ?>

    <?php if(get_field('saving_footer_button_link')): ?>
       <div class="saving-cta__hotels background-color__titles padding-top__mega padding-bottom__mega">
              <div class="container-fluid wrap">
                  <div class="row middle-xs center-xs around-sm">
                      <div class="col-xs-11 col-sm-6 center-xs start-sm">
                          <?php the_field('saving_footer_text'); ?>
                      </div>
                      <div class="col-xs-12 col-sm-3 center-xs end-sm">
                          <a href="<?php the_field('saving_footer_button_link'); ?>" class="btn saving-cta__btn"><?php the_field('saving_footer_button_text'); ?></a>
                      </div>
                  </div>
              </div>
          </div>
      <?php endif; ?>

    <div class="saving-shares">
        <a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href),'facebook-share-dialog','width=626,height=436');return false;" class="facebook"><i class="fab fa-facebook-f"></i></a>
        <a target="_blank" href="https://twitter.com/intent/tweet?status=<?php the_permalink(); ?>" class="twitter"><i class="fab fa-twitter"></i></a>
        <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" class="linkedin"><i class="fab fa-linkedin-in"></i></a>
    </div>
</section>


<?php endwhile; get_footer();
