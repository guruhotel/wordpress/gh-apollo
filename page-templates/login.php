<?php
/* Template name: Login */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="login">
    <div class="container-fluid wrap padding-top__section padding-bottom__section">
        <div class="row center-xs middle-xs">
            <div class="col-xs-12 col-md-6 center-xs">
                <a href="<?php bloginfo('wpurl'); ?>">
                    <img class="logo margin-bottom__mega--x" src="<?php bloginfo('template_directory'); ?>/assets/images/guruhotelPMS.svg">
                </a>
                <div class="card start-xs background-color__white border-radius__small--x box-shadow__medium border-color__grey--regent card__size--big">
                    <form role='form' action="https://admin.guruhotel.com/pms/hotels/login/access/" method="post">

                        <div class="margin-top__medium">
                            <label for="username">
                                <span><?php _e('Usuario', 'guruhotel'); ?></span>
                                <input type="text" class="input text-align__left input__size--big input__width--full background-color__white border-color__grey--regent box-shadow__normal border-radius__small--x margin-top__small--x font-weight__normal" name="username" id="username" placeholder="Usuario">
                            </label>
                        </div>

                        <div class="margin-top__medium">
                            <label for="password">
                                <span><?php _e('Contraseña', 'guruhotel'); ?></span>
                                <input type="password" class="input input__size--big input__width--full background-color__white border-color__grey--regent box-shadow__normal border-radius__small--x margin-top__small--x font-weight__normal" name="password" id="password" placeholder="Contraseña">
                            </label>
                        </div>

                        <button type="submit" class="border-radius__small--x btn btn__size--big background-color__main text-color__white padding__medium--x display__inline--block btn__width--full margin-top__big"><?php _e('Conectarse', 'guruhotel'); ?></button>
                        <div class="center-xs margin-top__big">
                            <span><?php _e('¿No tienes cuenta aún?', 'guruhotel'); ?></span> <a href="<?php bloginfo('wpurl'); ?>/planes" class="highlight text-color__main border-color__main"><?php _e('Únete a GuruHotel', 'guruhotel'); ?></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    window.addEventListener("message", function (event){
        const allowedOrigins = ['https://app.guruhotel.com','https://dev.guruhotel.com']
        const { username, password} = event.data
        if (!username  && !password) return;
        if (!allowedOrigins.includes(event.origin)) return;
        document.getElementById('username').value = username
        document.getElementById('password').value = password

        event.source.postMessage({ success: true}, '*');
        document.querySelector('#login > div > div > div > div > form > button').click()
    }, false);
</script>
<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php
get_footer();
