<?php
/**
 * Template Name: Blocks
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    <div class="page-wrapper <?php if(get_field('page_bg_white')) echo 'page--bg-white'; ?>">
        <?php the_content(); ?>
    </div>
<?php endwhile; get_footer();
