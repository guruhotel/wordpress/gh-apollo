<?php
/**
 * Template Name: Basic Landing
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
        <section class="home__main-banner padding__section text-color__white section-intro background-color__titles">
            <div class="container-fluid wrap">
                <img src="<?php $img = get_field('about_bg'); echo $img['sizes']['large']; ?>" class="bg-videos">
                <div class="row">
                    <div class="col-xs-11 col-md-8 col-sm-5 col-md-offset-0 col-sm-offset-1">
                        <?php if(get_field('about_subline')): ?>
                            <h4 class="font-size__small--x pretitle text-color__orange without-margin__bottom"><?php the_field('about_subline'); ?></h4>
                        <?php endif; ?>

                        <?php if(get_field('about_title')): ?>
                            <h1 class="font-size__mega--x text-color__white" data-aos="fade-up" data-aos-delay="200"><?php the_field('about_title'); ?></h1>
                        <?php endif; ?>
                    </div>

                    <div class="col-xs-11 col-md-6 col-sm-5 col-md-offset-0 col-sm-offset-1">
                        <?php if(get_field('about_text')): ?>
                            <div data-aos="fade-up" data-aos-delay="400">
                                <?php the_field('about_text'); ?>
                            </div>
                        <?php endif; ?>

                    </div>

                    <div class="col-xs-11 col-md-8 col-sm-5 col-md-offset-0 col-sm-offset-1">
                        <footer>
                            <a href="<?php the_field('about_cta_link'); ?>" class="btn btn--primary border-radius__mega--x background-color__white text-transform__uppercase letter-spacing__medium font-weight__medium text-color__titles padding__medium--x display__inline--block margin-top__mega banner-button">
                                <?php the_field('about_cta_text'); ?>
                            </a>

                            <?php if(get_field('about_cta_secondary_text')): ?>
                                <a href="<?php the_field('about_cta_secondary_link'); ?>" class="text-color__white btn--text margin-left__mega--x display__inline--block" data-lity><?php if(get_field('about_cta_secondary_icon')): ?>
                                <i class="<?php the_field('about_cta_secondary_icon'); ?> margin-right__normal"></i><?php endif; ?><?php the_field('about_cta_secondary_text'); ?></a>
                            <?php endif; ?>
                        </footer>
                    </div>
                </div>
            </div>
    </section>

    <div id="page-content">
        <div class="container-fluid wrap">
            <div class="row center-xs">
                <div class="col-xs-11 col-md-10 start-xs">
                    <?php $i=1; if(have_rows('sections')) : while(have_rows('sections')): the_row(); ?>
                        <section class="normal-section">
                            <div class="row">
                                <div class="col-xs-12 col-md-8">
                                    <?php if(get_sub_field('pretitle')): ?>
                                    <h4 class="font-size__small--x pretitle text-color__orange without-margin__bottom"><?php the_sub_field('pretitle'); ?></h4>
                                    <?php endif; ?>

                                    <?php if(get_sub_field('title')): ?>
                                        <h2 class="text-color__titles"><?php the_sub_field('title'); ?></h2>
                                    <?php endif; ?>

                                    <?php if(get_sub_field('text')): ?>
                                        <div class="font-size__medium"><?php the_sub_field('text'); ?></div>
                                    <?php endif; ?>

                                    <?php if(get_sub_field('cta_link')): ?>
                                        <a href="<?php the_sub_field('cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__mega--x background-color__main text-transform__uppercase letter-spacing__medium font-weight__medium text-color__white padding__medium--x display__inline--block margin-top__mega banner-button">
                                        <?php the_sub_field('cta_title'); ?>
                                    </a>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="row center-xs">
                                <div class="col-xs-12 col-md-10 start-xs">
                                    <?php if(get_sub_field('embed')): ?>
                                    <div class="video-embed border-radius__normal">
                                        <?php the_sub_field('embed'); ?>
                                    </div>
                                <?php endif; ?>

                                <?php if(get_sub_field('image')): ?>
                                    <div class="section-image margin-top__mega--x border-radius__normal">
                                       <img src="<?php $img = get_sub_field('image'); echo $img['sizes']['large']; ?>">
                                    </div>
                                <?php endif; ?>

                                <?php if(get_sub_field('code')): ?>
                                    <div class="border-radius__normal section-code">
                                       <?php the_sub_field('code'); ?>
                                    </div>
                                <?php endif; ?>
                                </div>
                            </div>

                        </section>
                    <?php $i++; endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php if(get_field('cta_title')): ?>
        <section id="home__cta" class="padding-bottom__small-section background-color__grey">
            <div class="container-fluid wrap">
                <div class="row middle-xs center-xs">
                    <div class="col-xs-11">

                        <?php if(get_field('cta_title')): ?>
                            <h2 class="font-size__mega"><?php the_field('cta_title'); ?></h2>
                        <?php endif; ?>

                        <?php if(get_field('cta_text')): ?>
                            <?php the_field('cta_text'); ?>
                        <?php endif; ?>

                        <footer>
                            <a href="<?php the_field('cta_cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__mega--x background-color__main text-transform__uppercase letter-spacing__medium font-weight__medium text-color__white padding__medium--x display__inline--block margin-top__mega banner-button">
                                <?php the_field('cta_cta_text'); ?>
                            </a>

                            <?php if(get_field('intro_cta_secondary_text')): ?>
                                <a href="<?php the_field('cta_cta_secondary_link'); ?>" class="text-color__text btn--text margin-left__mega--x display__inline--block" data-lity><i class="fa fa-play-circle margin-right__normal"></i><?php the_field('cta_cta_secondary_text'); ?></a>
                            <?php endif; ?>
                        </footer>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

<?php endwhile; get_footer();
