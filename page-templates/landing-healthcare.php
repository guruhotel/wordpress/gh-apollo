<?php
/*
* Template name: Landings Page Healthcare Workers
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <section id="healthcare--landing">
        <div class="healthcare--landing__banner padding-top__small-section padding-bottom__small-section margin-top__mega--x">
            <?php the_post_thumbnail( 'full', array('class' => 'healthcare--landing__banner--bg border-radius__big') ); ?>

            <div class="container-fluid wrap" style="position: relative; z-index: 444;">
                <div class="row center-xs start-md">
                    <div class="col-xs-12 col-md-6 center-xs start-md">
                        <div class="banner-text">

                            <?php if(get_field('gurucare_intro_title')): ?>
                                <h1 class="banner--title font-size__mega--x line-height__medium--x"><?php the_field('gurucare_intro_title'); ?></h1>
                            <?php endif; ?>

                            <?php if(get_field('gurucare_intro_text')): ?>
                                <div class="banner--desc"><?php the_field('gurucare_intro_text'); ?></div>
                            <?php endif; ?>

                            <?php if(get_field('gurucare_intro_coupon_code')): ?>
                                <div class="banner--promo border-radius__medium margin-top__medium padding-top__medium padding-bottom__medium padding-left__big padding-right__big">
                                    <span class="label display__block font-size__small--x">Código del cupón:</span>
                                    <span class="coupon display__block font-size__medium--x text-color__titles"><?php the_field('gurucare_intro_coupon_code'); ?></span>
                                </div>
                            <?php endif; ?>
                        </div>

                        <?php if(have_rows('gurucare_features')) : ?>
                            <div class="banner--features">
                                <div class="row start-xs">
                                <?php while(have_rows('gurucare_features')): the_row(); ?>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="feature--card background-color__white border-radius__medium display__block margin-bottom__mega--x padding__big">
                                            <i class="<?php the_sub_field('icon'); ?> font-size__big"></i>
                                            <h2 class="title font-size__medium line-height__big"><?php the_sub_field('title'); ?></h2>
                                            <p class="desc"><?php the_sub_field('text'); ?></p>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if (have_rows('gurucare_partners')): ?>
            <section id="partners" class="margin-top__mega--x margin-bottom__mega--x">
                <div class="container-fluid wrap">

                    <?php if(get_field('gurucare_partners_title')): ?>
                        <div class="row center-xs">
                           <div class="col-xs-12 col-md-5">
                               <p class="title text-color__text font-size__medium"><?php the_field('gurucare_partners_title'); ?></p>
                           </div>
                       </div>
                    <?php endif; ?>

                    <div class="row center-xs middle-xs">
                        <div class="col-xs-12 logos">
                            <div class="row center-xs middle-xs">
                                <?php $logo_count = 1; while(have_rows('gurucare_partners')): the_row(); ?>
                                    <div class="logo col-xs-4 col-md-2 margin-top__medium--x margin-bottom__medium--x <?php if ($logo_count > 9) echo 'hidden__xs'; ?>">
                                        <a href="<?php the_sub_field('link'); ?>" target="_blank" rel="nofollow"><img src="<?php $img = get_sub_field('logo'); echo $img['sizes']['medium']; ?>"></a>
                                    </div>
                                <?php $logo_count++; endwhile;?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>
    </section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php
get_footer();
