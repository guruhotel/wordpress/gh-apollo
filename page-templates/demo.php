<?php
/**
 * Template Name: Demo
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
        <section class="home__main-banner padding__section text-color__white section-intro background-color__titles">
            <div class="container-fluid wrap">
                <img src="<?php $img = get_field('about_bg'); echo $img['sizes']['large']; ?>" class="bg-videos">
                <div class="row">
                    <div class="col-xs-11 col-md-5 col-sm-5 col-md-offset-0 col-sm-offset-1">
                        <?php if(get_field('about_subline')): ?>
                            <h4 class="font-size__small--x pretitle text-color__orange without-margin__bottom"><?php the_field('about_subline'); ?></h4>
                        <?php endif; ?>

                        <?php if(get_field('about_title')): ?>
                            <h1 class="font-size__mega--x text-color__white" data-aos="fade-up" data-aos-delay="200"><?php the_field('about_title'); ?></h1>
                        <?php endif; ?>

                        <?php if(get_field('about_text')): ?>
                            <div data-aos="fade-up" data-aos-delay="400">
                                <?php the_field('about_text'); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-11 col-sm-4 col-md-5 col-sm-offset-1 col-md-offset-2">
                        <div class="videos-links">
                            <ul class="list-unstyled">
                                <?php $i=1; if(have_rows('videos')) : while(have_rows('videos')): the_row(); ?>
                                    <li class="item">
                                        <a href="#video-<?php echo $i; ?>" class="text-color__white"><i class="far fa-play-circle font-size__mega--x margin-right__big"></i> <?php the_sub_field('pretitle'); ?></a>
                                    </li>
                                <?php $i++; endwhile; endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    </section>


    <section class="section-videos">

        <?php $i=1; if(have_rows('videos')) : while(have_rows('videos')): the_row(); ?>
            <div class="video-subsection" id="video-<?php echo $i; ?>">
                <div class="container-fluid wrap">
                    <div class="row">
                        <div class="col-xs-2 col-sm-1">
                            <i class="far fa-play-circle font-size__mega--x text-color__orange"></i>
                        </div>

                        <div class="col-xs-10 col-sm-11 col-sm-8 col-md-4 start-xs">
                            <?php if(get_sub_field('pretitle')): ?>
                            <h4 class="font-size__small--x pretitle text-color__orange without-margin__bottom"><?php the_sub_field('pretitle'); ?></h4>
                            <?php endif; ?>

                            <?php if(get_sub_field('title')): ?>
                                <h2 class="text-color__titles"><?php the_sub_field('title'); ?></h2>
                            <?php endif; ?>

                            <?php if(get_sub_field('text')): ?>
                                <div class="font-size__medium"><?php the_sub_field('text'); ?></div>
                            <?php endif; ?>

                            <?php if(get_sub_field('cta_link')): ?>
                                <a href="<?php the_sub_field('cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__mega--x background-color__main text-transform__uppercase letter-spacing__medium font-weight__medium text-color__white padding__medium--x display__inline--block margin-top__mega banner-button">
                                <?php the_sub_field('cta_title'); ?>
                            </a>
                            <?php endif; ?>


                        </div>

                        <div class="col-xs-12 col-md-6 col-md-offset-1">
                            <div class="video-embed border-radius__normal">
                                <?php the_sub_field('embed'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php $i++; endwhile; endif; ?>
    </section>

    <section id="home__cta" class="padding-bottom__small-section background-color__grey">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-11">

                    <?php if(get_field('cta_title')): ?>
                        <h2 class="font-size__mega"><?php the_field('cta_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('cta_text')): ?>
                        <?php the_field('cta_text'); ?>
                    <?php endif; ?>

                    <footer>
                        <a href="<?php the_field('cta_cta_link'); ?>" class="btn btn--primary btn-primary btn--primary border-radius__mega--x background-color__main text-transform__uppercase letter-spacing__medium font-weight__medium text-color__white padding__medium--x display__inline--block margin-top__mega banner-button">
                            <?php the_field('cta_cta_text'); ?>
                        </a>

                        <?php if(get_field('intro_cta_secondary_text')): ?>
                            <a href="<?php the_field('cta_cta_secondary_link'); ?>" class="text-color__text btn--text margin-left__mega--x display__inline--block" data-lity><i class="fa fa-play-circle margin-right__normal"></i><?php the_field('cta_cta_secondary_text'); ?></a>
                        <?php endif; ?>
                    </footer>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; get_footer();
