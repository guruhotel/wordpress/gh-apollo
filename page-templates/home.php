<?php
/**
 * Template Name: Home
 */
get_header();

$image_suffix = '';

if(is_page(1092)){
    $image_suffix = '-mastercard';
}
?>

<?php while (have_posts()) : the_post(); ?>
    <section id="home__main-banner" class="padding-bottom__section">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-12">
                    <div class="banner-text center-xs">

                        <?php if(get_field('intro_preimage')): ?>
                            <img src="<?php $img = get_field('intro_preimage'); echo $img['sizes']['medium_large']; ?>" class="margin-bottom__mega--x margin-top__mega--x">
                        <?php endif; ?>

                        <div style="display:block; margin: 0 auto 40px auto; text-align: center;">
                            <a href="https://www.producthunt.com/posts/guruhotel?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-guruhotel" target="_blank"><img src="https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=228342&theme=dark" alt="GuruHotel - The ultimate e-commerce platform for hotels | Product Hunt Embed" style="width: 250px; height: 54px;" width="250px" height="54px" /></a>
                        </div>

                        <?php if (get_field('intro_pretitle')): ?>
                            <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase" <?php if(is_front_page()) echo 'data-aos="fade-up" data-aos-delay="200"'; ?>><?php the_field('intro_pretitle'); ?></h4>
                        <?php endif ?>

                        <?php if(get_field('intro_title_first_part')): ?>
                           <h1 class="banner-title" <?php if(is_front_page()) echo 'data-aos="fade-up" data-aos-delay="400"'; ?>><?php the_field('intro_title_first_part'); ?> <span class="text-color__main display__block"><?php the_field('intro_title_second_part'); ?></span></h1>
                        <?php endif; ?>

                        <?php if(get_field('intro_text')): ?>
                           <p class="banner-desc font-size__medium text-color__titles" <?php if(is_front_page()) echo 'data-aos="fade-up" data-aos-delay="600"'; ?>><?php the_field('intro_text'); ?></p>
                       <?php endif; ?>

                       <?php if(get_field('intro_extra_text')): ?>
                            <div class="banner-extra-text margin-top__mega--x" <?php if(is_front_page()) echo 'data-aos="fade-up" data-aos-delay="600"'; ?>>
                               <?php the_field('intro_extra_text'); ?>
                           </div>
                       <?php endif; ?>

                       <?php if(get_field('intro_cta_link')): ?>
                            <a href="<?php the_field('intro_cta_link'); ?>" class="btn btn--primary btn-primary btn__size--mega   btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega font-size__small--x banner-button" <?php if(is_front_page()) echo ' data-aos="fade-up" data-aos-delay="800"'; ?>>
                                <?php the_field('intro_cta_text'); ?>
                            </a>
                       <?php endif; ?>

                    </div>

                    <div class="banner-image">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/website-guruhotel<?php echo $image_suffix; ?>.svg" class="banner--image__website" alt="GuruHotel Hotels Website">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/website-guruhotel__booking.svg" class="banner--image__app booking" data-aos="fade-left" data-aos-delay="400" alt="GuruHotel Hotels Apps Booking.com">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/website-guruhotel__google.svg" class="banner--image__app google wow " data-aos="fade-left" data-aos-delay="200" alt="GuruHotel Hotels Apps Google" >
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/website-guruhotel__pms.svg" class="banner--image__app pms wow " data-aos="fade-left" data-aos-delay="100" alt="GuruHotel Hotels Apps PMS">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/website-guruhotel__checkout<?php echo $image_suffix; ?>.svg" class="banner--image__checkout wow " data-aos="fade-right" data-aos-delay="500" alt="GuruHotel Hotels Checkout">
                    </div>
                </div>

                <?php if(get_field('intro_integrations_logos')) : ?>
                    <div class="col-sm-12 col-md-10 hidden__xs hidden__sm">
                        <div class="banner-integrations margin-top__mega--x">
                            <div class="row between-xs middle-xs">
                                <?php
                                    $logo_count = 1;
                                    $integrations_logos = get_field('intro_integrations_logos');
                                    foreach ($integrations_logos as $logo):
                                 ?>
                                    <div class="col" data-aos="fade-up" data-aos-delay="<?php echo $logo_count * 50; ?>">
                                        <img src="<?php echo $logo['sizes']['medium']; ?>" alt="<?php echo $logo['alt']; ?>">
                                    </div>
                                <?php $logo_count++; endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if(get_field('intro_integrations_text')): ?>
                    <div class="col-xs-8 col-md-6 margin-top__mega--x margin-bottom__mega--x text-color__titles">
                        <p class="banner-integrations-text"><?php the_field('intro_integrations_text'); ?></p>
                    </div>
                <?php endif; ?>


                <?php if(have_rows('intro_features')) : ?>
                    <div class="col-xs-12">
                        <div class="banner-features">
                            <div class="row">
                                <?php
                                    $features_count = 1;
                                    while(have_rows('intro_features')): the_row();
                                ?>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <div class="card shadow__inset start-xs border-radius__small--x padding__mega background-color__white margin-top__mega--x" data-aos="fade-up" data-aos-delay="<?php echo $features_count*50; ?>">
                                            <div class="row middle-xs">
                                                <div class="col-xs-3 col-sm-4">
                                                    <span class="padding__medium--x background-color__main border-radius__medium display__inline--block vertical-align__middle">
                                                        <i class="<?php the_sub_field('icon'); ?> text-color__white font-size__mega"></i>
                                                    </span>
                                                </div>
                                                <div class="col-xs-9 col-sm-8">
                                                    <h4><?php the_sub_field('text'); ?></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $features_count++; endwhile; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <?php if(get_field('banner_text')): ?>
        <section id="home__covid" class="background-color__main padding__section">

            <div class="container-fluid wrap">
                <div class="row center-xs">
                    <div class="col-xs-11 col-xs-offset-1 col-md-8 col-lg-8 col-md-offset-1">

                        <?php if(get_field('banner_title')): ?>
                            <h2 class="text-color__white"><?php the_field('banner_title'); ?></h2>
                        <?php endif; ?>

                        <?php if(get_field('banner_text')): ?>
                            <div class="text-color__white"><?php the_field('banner_text'); ?></div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <section id="home-steps" class="padding__section">
        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-7">
                    <?php if(get_field('steps_pretitle')): ?>
                        <h4 class="font-size__small--x letter-spacing__big text-transform__uppercase"><?php the_field('steps_pretitle'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('steps_title')): ?>
                        <h2><?php the_field('steps_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('steps_text')): ?>
                        <?php the_field('steps_text'); ?>
                    <?php endif; ?>
                </div>
            </div>

            <?php if(have_rows('steps')) : ?>

            <div class="row margin-top__mega--x">
                <div class="col-xs-7 col-xs-offset-4 col-md-4 col-md-offset-1">
                    <?php
                        $steps_count = 1;
                        while(have_rows('steps')): the_row();
                    ?>
                        <div class="steps__step steps__step--active" data-target=".steps__illustration--<?php echo $steps_count; ?>" data-aos="fade-up">
                            <span class="icon shadow__inset padding__medium--x background-color__white display__inline--block">
                                <i class="<?php the_sub_field('icon'); ?> font-size__big"></i>
                            </span>
                            <h4><?php the_sub_field('title'); ?></h4>
                            <p><?php the_sub_field('text'); ?></p>
                        </div>
                    <?php $steps_count++; endwhile; ?>
                </div>

                <div class="col-md-7 col-lg-6 col-lg-offset-1 hidden__xs hidden__sm">
                    <div class="steps__illustrations">
                        <div class="steps__illustration steps__illustration--active steps__illustration--1">
                             <img src="<?php bloginfo('template_directory'); ?>/assets/images/home-step-1<?php echo $image_suffix; ?>.svg">
                        </div>
                        <div class="steps__illustration steps__illustration--2">
                             <img src="<?php bloginfo('template_directory'); ?>/assets/images/home-step-2<?php echo $image_suffix; ?>.svg">
                        </div>
                        <div class="steps__illustration steps__illustration--3">
                             <img src="<?php bloginfo('template_directory'); ?>/assets/images/home-step-3<?php echo $image_suffix; ?>.svg">
                        </div>
                    </div>
                </div>
            </div>

        <?php endif; ?>

        <?php if(get_field('steps_cta_link')): ?>
            <div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-1">
                <a href="<?php the_field('steps_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('steps_cta_text'); ?></a>
            </div>
        <?php endif; ?>

        </div>
    </section>

    <section id="home-marketplace" class="background-color__main">
        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-md-5 col-md-offset-0 padding__section text-color__white">
                    <div class="marketplace-text">
                        <?php if(get_field('marketplace_pretitle')): ?>
                            <h4 class="font-size__small--x letter-spacing__big text-transform__uppercase text-color__white"><?php the_field('marketplace_pretitle'); ?></h4>
                        <?php endif; ?>

                        <?php if(get_field('marketplace_title')): ?>
                            <h2 class="text-color__white"><?php the_field('marketplace_title'); ?></h2>
                        <?php endif; ?>

                        <?php if(get_field('marketplace_text')): ?>
                            <?php the_field('marketplace_text'); ?>
                        <?php endif; ?>

                        <?php if(get_field('marketplace_cta_link')): ?>
                            <a href="<?php the_field('marketplace_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('marketplace_cta_text'); ?></a>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>

        <?php
            if (have_rows('marketplace_cards')):
        ?>
            <div class="marketplace-cards col-md-7 col-lg-5 col-xs-12 hidden__xs">
                <div class="row">
                    <?php for ($i=0; $i < 2; $i++): ?>
                        <?php while(have_rows('marketplace_cards')): the_row(); ?>
                            <div class="card background-color__white border-radius__normal display__inline--block margin-bottom__big  padding__mega--x" data-aos="fade-up">
                                <div class="row middle-xs">
                                    <div class="col-xs-9">
                                        <img src="<?php $img = get_sub_field('logo'); echo $img['sizes']['medium']; ?>">
                                    </div>
                                    <div class="col-xs-3">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/toggle-on.svg">
                                    </div>
                                </div>
                                <p class="font-size__small--x"><?php the_sub_field('text'); ?></p>
                            </div>
                        <?php endwhile;?>
                    <?php endfor; ?>
                </div>
            </div>
        <?php endif ?>
    </section>

    <section class="padding__section padding-bottom__small-section position__relative background-color__grey" id="home-checkout">
        <div class="container-fluid wrap">
            <div class="row middle-xs">
                <div class="col-xs-10 col-xs-offset-1 col-md-offset-0 col-md-6">
                    <div class="home-checkout-animation position__relative">
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/checkout<?php echo $image_suffix; ?>.svg" alt="">
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/checkout-icon-1.svg"  data-aos="fade-up" data-aos-delay="100">
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/checkout-icon-2.svg"  data-aos="fade-down" data-aos-delay="200">
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/checkout-icon-3.svg"  data-aos="fade-up" data-aos-delay="300">
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/checkout-icon-4<?php echo $image_suffix; ?>.svg" data-aos="fade-down" data-aos-delay="400">
                    </div>

                </div>
                <div class="col-xs-10 col-xs-offset-1 col-md-5 col-md-offset-1">
                    <?php if(get_field('checkout_pretitle')): ?>
                        <h4 class="font-size__small--x letter-spacing__big text-transform__uppercase"><?php the_field('checkout_pretitle'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('checkout_title')): ?>
                        <h2><?php the_field('checkout_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('checkout_text')): ?>
                        <?php the_field('checkout_text'); ?>
                    <?php endif; ?>

                    <?php if(get_field('checkout_cta_link')): ?>
                        <a href="<?php the_field('checkout_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('checkout_cta_text'); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <?php if (get_field('show_quotes')): ?>

        <section id="home-quotes" class="background-color__titles position__relative">
            <div class="container-fluid wrap">
                <div class="row middle-xs center-xs">
                    <div class="col-xs-10">
                        <div class="quotes-logos">
                            <?php
                                $i = 1;
                                $args = array(
                                  'post_type'      => 'guru_case_study',
                                  'posts_per_page' => 8,
                                );
                                $query = new WP_Query( $args );
                                if($query->have_posts()): while($query->have_posts()) : $query->the_post();
                            ?>
                                <a href="#">
                                    <img src="<?php $img = get_field('logo'); echo $img['sizes']['medium']; ?>" data-aos="fade-up" data-aos-delay="<?php echo $i*150; ?>">
                                </a>
                            <?php $i++; endwhile; endif; ?>
                        </div>

                        <div class="quotes-slider">
                            <?php
                                while($query->have_posts()) : $query->the_post();
                            ?>
                                <div class="quotes-slider__slide background-color__white border-radius__medium--x left-xs">
                                    <div class="row">
                                        <div class="col-xs-10 order-xs__2 order-sm__2 col-xs-offset-2 col-md-7 col-md-offset-1 padding-top__mega--x padding-bottom__mega--x quotes-slider__content">
                                            <blockquote class="without-margin__left position__relative">
                                                <?php echo strip_tags(get_the_content(
                                            )); ?>
                                            </blockquote>

                                            <footer><h6 class="display__inline--block"><?php the_field('name'); ?></h6> - <h6 class="display__inline--block"><?php the_title(); ?></h6></footer>
                                        </div>
                                        <div class="col-xs-12 order-xs__1 order-sm__1 col-md-4 position__relative quotes-slider__images">

                                            <?php the_post_thumbnail('medium_large', array('class'=> 'quotes-slider__img--hotel')); ?>

                                            <img src="<?php $img = get_field('logo'); echo $img['sizes']['medium']; ?>" class="quotes-slider__img--logo" data-aos="zoom-in">
                                        </div>
                                    </div>
                                </div>
                           <?php endwhile; wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif ?>

    <section id="home__cta" class="padding-top__small-section padding-bottom__mega--x background-color__grey">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-10 col-md-6">

                    <?php if (get_field('cta_title_img')): ?>

                        <img src="<?php $img = get_field('cta_title_img'); echo $img['sizes']['medium_large']; ?>">

                    <?php elseif(get_field('cta_title')): ?>
                        <h2 class="font-size__mega"><?php the_field('cta_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('cta_text')): ?>
                        <?php the_field('cta_text'); ?>
                    <?php endif; ?>

                    <?php if(get_field('cta_cta_link')): ?>
                        <a href="<?php the_field('cta_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('cta_cta_text'); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; get_footer();
