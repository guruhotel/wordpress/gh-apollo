<?php
/* Template name: Landings Page Template */
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section id="page-template-landing">
    <div class="background-color__titles hero-page  padding-top__section">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs between-sm">
                <div class="col-xs-12 col-sm-6 col-md-6 center-xs start-sm padding-bottom__section">
                    <div class="hero-caption center-xs start-sm">
                        <h1 class="font-size__mega--x center-xs start-sm text-color__white without-margin-top" data-aos="fade-up"><?php the_title(); ?></h1>

                        <?php if(get_field('landing_subline')): ?>
                           <p class="center-xs font-size__medium  start-sm margin-top__medium margin-bottom__medium text-color__white" data-aos="fade-up" data-aos-delay="0.2s"><?php the_field('landing_subline'); ?></p>
                        <?php endif; ?>

                        <?php if(get_field('landing_scroll_btn')): ?>
                            <a href="#contact-form" class="btn btn--primary background-color__yellow font-size__normal big text-color__titles border-radius__small--x padding__medium display__inline--block margin-top__medium"  data-aos="fade-up" data-aos-delay="0.4s"><?php the_field('landing_scroll_btn'); ?></a>
                        <?php endif; ?>
                    </div>
                </div>

                <?php if(get_field('landing_banner_img')): ?>
                    <div class="col-xs-12 hero-img col-sm-6 position-relative">
                        <img src="<?php $img = get_field('landing_banner_img'); echo $img['sizes']['medium_large'];  ?>" data-aos="fade-zoom-in" data-aos-delay="0.6s">
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </div>

    <?php if(have_rows('landing_main_features')) : ?>
        <div class="container-fluid wrap template-landing-main-features margin-top__mega--x margin-bottom__mega--x">
            <div class="row center-xs">
                <?php while(have_rows('landing_main_features')): the_row(); ?>
                    <div class="feature col-xs-12 col-sm-8 col-md-4">
                        <div class="card padding__big border-color__grey--regent box-shadow__medium start-xs">
                            <i class="<?php the_sub_field('icon'); ?> icon font-size-x-medium main-color"></i>
                            <h3 class="font-size__medium margin-top__normal">
                                <?php the_sub_field('title'); ?>
                            </h3>
                            <p class="text-color__titles"><?php the_sub_field('text'); ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    <?php endif; ?>

    <div id="contact-form" class="background-color__white padding-top__small-section padding-bottom__small-section">
        <div class="container-fluid wrap">
            <div class="row center-xs between-md">
                <div class="the-content start-xs col-xs-12 col-md-5 text-color__text" data-aos="fade-up">
                    <?php
                    $form_content = get_field('form_content_some_desc');
                    if( !empty($form_content) ) {
                        echo wpautop($form_content);
                    } ?>
                </div>
                <div class="col-xs-12 col-md-6 start-xs">
                    <div id="page-form" class="card rounded-small page-form background-color__titles text-color__white titles-border" data-aos="fade-up" data-aos-delay="0.3s">
                        <?php
                        $form_shortcode = get_field('landing_form_shortcode');
                        if( $form_shortcode ) {
                            echo do_shortcode($form_shortcode);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid wrap">
        <div class="row center-xs">
            <div class="col-xs-12 col-md-5 start-xs">
                <div class="the-content"><?php the_content(); ?></div>
            </div>
        </div>
    </div>
</section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php
get_footer();
