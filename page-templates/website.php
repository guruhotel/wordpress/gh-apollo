<?php
/**
 * Template Name: Product - Website
 */
get_header();
while(have_posts()): the_post();
?>

    <img src="<?php bloginfo('template_directory'); ?>/src/images/website-landing-illustration.svg" class="website-intro__animation hidden__xs hidden__sm" id="website-intro__animation">

    <div class="website-intro section-intro background-color__main padding-bottom__small-section padding-top__section">

        <div class="website-intro__circle" id="website-intro__circle"></div>

        <div class="current-webpage-widget text-color__white hidden__xs">
            <div class="current-webpage-widget__inner border-radius__normal">
                <ul>
                    <li <?php if (is_page_template('page-templates/website.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1250); ?>"><i class="fas fa-globe-americas"></i> Website</a>
                    </li>
                    <li <?php if (is_page_template('page-templates/checkout.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1209); ?>"><i class="fas fa-shopping-cart"></i> Checkout</a>
                    </li>
                    <li <?php if (is_page_template('page-templates/marketplace.php')) echo 'class="active"'; ?>>
                        <a href="<?php the_permalink(1119); ?>"><i class="fas fa-toggle-on"></i> Marketplace</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container-fluid wrap">
            <div class="row middle-xs">
                <div class="col-xs-11 col-md-5 col-md-offset-1 text-color__white">

                    <?php if(get_field('website_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__white line-height__medium--x" data-aos="fade-up" data-aos-delay="200"><?php the_field('website_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('website_title')): ?>
                        <h1 class="font-size__mega--x text-color__white" data-aos="fade-up" data-aos-delay="200"><?php the_field('website_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('website_text')): ?>
                        <div data-aos="fade-up" data-aos-delay="400">
                            <?php the_field('website_text'); ?>
                        </div>
                    <?php endif; ?>

                    <footer data-aos="fade-up" data-aos-delay="600">
                       <?php if(get_field('website_intro_cta_link')): ?>
                           <a href="<?php the_field('website_intro_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__white text-color__main padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('website_intro_cta_text'); ?></a>
                       <?php endif; ?>

                       <?php if(get_field('website_intro_alt_cta_link')): ?>
                           <a href="<?php the_field('website_intro_alt_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><i class="<?php the_field('website_intro_alt_cta_icon'); ?>"></i><?php the_field('website_intro_alt_cta_text'); ?></a>
                       <?php endif; ?>
                    </footer>
                </div>
            </div>
        </div>

            <img src="<?php bloginfo('template_directory'); ?>/src/images/website-landing-illustration.svg" class="website-intro__animation hidden__md hidden__lg">
    </div>



    <div class="website-stats padding-top__section padding-bottom__small-section">
        <div class="container-fluid wrap">
            <div class="row center-xs middle-xs">
                <div class="col-xs-11 col-md-5 start-xs">

                    <?php if(get_field('website_stats_subline')): ?>
                       <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase text-color__titles" data-aos="fade-up"><?php the_field('website_stats_subline'); ?></h4>
                    <?php endif; ?>

                    <?php if(get_field('website_stats_title')): ?>
                        <h1 class="font-size__mega text-color__titles" data-aos="fade-up"><?php the_field('website_stats_title'); ?></h1>
                    <?php endif; ?>

                    <?php if(get_field('website_stats_text')): ?>
                        <p data-aos="fade-up">
                            <?php the_field('website_stats_text'); ?>
                        </p>
                    <?php endif; ?>
                </div>

                <div class="col-xs-11 col-md-6 col-md-offset-1 start-xs website_stats__cards">
                    <div class="row">
                        <?php $stat_count = 0; if(have_rows('website_stats')) : while(have_rows('website_stats')): the_row(); ?>
                            <article class="margin-bottom__big col-xs-6" data-aos="fade-up">
                                <div class="card border-radius__medium box-shadow__normal background-color__main center-xs text-color__white">
                                    <i class="icon text-color__white <?php the_sub_field('icon'); ?>"></i>
                                    <h4 class="font-size__mega text-color__white"><?php the_sub_field('title'); ?></h4>

                                    <span class="price font-size__medium text-color__white"><?php the_sub_field('subtitle'); ?></span>
                                </div>
                            </article>
                        <?php $stat_count++; endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if(have_rows('website_sections')) : ?>
        <div class="website-sections padding-bottom__small-section">
            <div class="container-fluid wrap">
                <?php $section_count = 0; while(have_rows('website_sections')): the_row(); ?>
                    <section class="padding-bottom__small-section">
                        <div class="row middle-xs">

                            <?php
                                if($section_count%2 == 0){
                                    $data_aos = 'fade-left';
                                }
                                else{
                                    $data_aos = 'fade-right';
                                }
                             ?>

                            <div class="col-xs-12 col-md-6 center-xs <?php if($section_count%2 == 0) echo 'col-md-offset-1 last-md'; ?>">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/website-section-<?php echo $section_count; ?>.svg" data-aos="<?php echo $data_aos; ?>">
                            </div>

                            <div class="col-xs-11 col-md-5 start-xs">
                                <?php if(get_sub_field('subline')): ?>
                                   <h4 class="banner-preline font-size__small--x letter-spacing__big text-transform__uppercase" data-aos="fade-up" data-aos-delay="200"><?php the_sub_field('subline'); ?></h4>
                                <?php endif; ?>

                                <?php if(get_sub_field('title')): ?>
                                    <h2 class="font-size__mega" data-aos="fade-up"><?php the_sub_field('title'); ?></h1>
                                <?php endif; ?>

                                <?php if(get_sub_field('text')): ?>
                                    <p data-aos="fade-up">
                                        <?php the_sub_field('text'); ?>
                                    </p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </section>
                <?php $section_count++; endwhile; ?>
            </div>
        </div>
    <?php endif; ?>

    <section id="home__cta" class="padding-top__section padding-bottom__mega--x background-color__grey">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-10 col-md-6">

                    <?php if (get_field('website_cta_title_img')): ?>

                        <img src="<?php $img = get_field('website_cta_title_img'); echo $img['sizes']['medium_large']; ?>">

                    <?php elseif(get_field('website_cta_title')): ?>
                        <h2 class="font-size__mega" data-aos="fade-up"><?php the_field('website_cta_title'); ?></h2>
                    <?php endif; ?>

                    <?php if(get_field('website_cta_text')): ?>
                        <?php the_field('website_cta_text'); ?>
                    <?php endif; ?>

                    <?php if(get_field('website_cta_cta_link')): ?>
                        <a href="<?php the_field('website_cta_cta_link'); ?>" class="btn  btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block margin-top__mega--x font-size__small--x"><?php the_field('website_cta_cta_text'); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; get_footer();
