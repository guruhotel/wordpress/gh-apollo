
    <?php if (!is_page_template('page-templates/login.php')): ?>
        <footer id="footer" class="padding-top__mega--x background-color__grey <?php if (get_field('show_white_footer')) echo 'footer__white'; ?>">

            <div class="footer-top container-fluid wrap padding-top__mega--x padding-bottom__mega--x">
                <div class="row between-xs">
                   <div class="brand col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 col-md-5 top-xs">

                        <?php if (is_page_template('landings-page-template-two.php') || is_page_template('landings-page-template.php') || is_page_template('gurupay.php')) : ?>
                            <a href="<?php bloginfo('wpurl'); ?>" class="display__inline--block margin-right__mega">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-inverse.svg" class="logo">
                            </a>
                        <?php else: ?>
                            <a href="<?php bloginfo('wpurl'); ?>" class="display__inline--block  margin-right__mega">
                               <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-original.svg" class="logo">
                            </a>
                        <?php endif; ?>

                        <ul class="list-unstyled social-nav list-horizontal display__inline--block margin-top__normal vertical-align__top ">
                            <?php if(get_field('facebook_url', 'option')): ?>
                                <li>
                                    <a href="<?php the_field('facebook_url', 'option'); ?>" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                           <?php endif; if(get_field('instagram_url', 'option')): ?>
                               <li>
                                   <a href="<?php the_field('instagram_url', 'option'); ?>" target="_blank">
                                       <i class="fab fa-instagram"></i>
                                   </a>
                               </li>
                           <?php endif; if(get_field('twitter_url', 'option')): ?>
                                <li>
                                    <a href="<?php the_field('twitter_url', 'option'); ?>" target="_blank">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                           <?php endif; if(get_field('linkedin_url', 'option')): ?>
                                <li>
                                    <a href="<?php the_field('linkedin_url', 'option'); ?>" target="_blank">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                           <?php endif; if(get_field('spotify_url', 'option')): ?>
                                <li>
                                    <a href="<?php the_field('spotify_url', 'option'); ?>" target="_blank">
                                        <i class="fab fa-spotify"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>

                        <div class="lang-selector">
                            <?php echo do_shortcode('[language-switcher]'); ?>
                        </div>

                        <p class="font-size__small--x margin-top__big without-margin__bottom"><?php _e('Sign up for updates', 'gh-apollo'); ?></p>

                        <?php echo do_shortcode('[wpforms id="2356" title="false" description="false"]'); ?>
                   </div>

                   <div class="menu col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-0 col-md-6">
                       <nav class="menu-footer">
                           <?php wp_nav_menu( array( 'theme-location' => 'menu-footer', 'container' => '' ) ); ?>
                       </nav>
                   </div>
               </div>
            </div>

            <div class="footer-bottom container-fluid wrap padding-bottom__mega--x text-color__titles font-size__small--x">
                <p class="font-size__small--x padding-top__medium">
                    <?php _e('© 2020 GuruHotel | The hospitality future', 'guru'); ?>
                    <a href="<?php echo get_privacy_policy_url(); ?>" class="btn--text margin-left__mega--x"><?php _e('Terms of service', 'understrap'); ?></a>
                </p>
            </div>

        </footer>
    <?php endif ?>
<?php wp_footer(); ?>
</body>
</html>
