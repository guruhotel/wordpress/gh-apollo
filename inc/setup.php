<?php

if ( ! function_exists( 'wordpress_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 */
	function wordpress_theme_setup() {

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
            'menu-main' => esc_html__( 'Primary menu', 'gh-apollo' ),
            'menu-footer' => esc_html__( 'Footer menu', 'gh-apollo' ),
		) );
		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

        //ACF
        if( function_exists('acf_add_options_page') ) {
            acf_add_options_page('Opciones');
        }
	}
endif;
add_action( 'after_setup_theme', 'wordpress_theme_setup' );

/**
 * Register widget area.
 *
 */
function wordpress_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gh-apollo' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gh-apollo' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wordpress_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wordpress_theme_scripts() {
    wp_enqueue_style( 'gh-apollo-style', get_stylesheet_uri() );
    wp_enqueue_script( 'gh-apollo-scripts', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'wordpress_theme_scripts' );

/**
 * CPTs
 */

//Case Studies
function custom_post_type_guru_case_study() {

  $labels = array(
    'name'                => _x( 'Case Studies', 'Post Type General Name', 'sage' ),
    'singular_name'       => _x( 'Case Study', 'Post Type Singular Name', 'sage' ),
    'menu_name'           => __( 'Case Studies', 'sage' ),
    'parent_item_colon'   => __( 'Parent Case Study', 'sage' ),
    'all_items'           => __( 'All Case Studies', 'sage' ),
    'view_item'           => __( 'View Case Studies', 'sage' ),
    'add_new_item'        => __( 'Add new Case Study', 'sage' ),
    'add_new'             => __( 'Add new', 'sage' ),
    'edit_item'           => __( 'Edit Case Study', 'sage' ),
    'update_item'         => __( 'Update Case Study', 'sage' ),
    'search_items'        => __( 'Search Case Study', 'sage' ),
    'not_found'           => __( 'Not Found', 'sage' ),
    'not_found_in_trash'  => __( 'Not found in Trash', 'sage' ),
  );

  $args = array(
    'label'               => __( 'Case Studies', 'sage' ),
    'description'         => __( 'Case Studies', 'sage' ),
    'labels'              => $labels,
    // Features this CPT supports in Post Editor
    'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
    // You can associate this CPT with a taxonomy or custom taxonomy.
    'taxonomies'          => array( 'genres' ),
    /* A hierarchical CPT is like Pages and can have
    * Parent and child items. A non-hierarchical CPT
    * is like Posts.
    */
    'hierarchical'        => false,
    'public'              => false,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'show_in_nav_menus'   => true,
    'show_in_admin_bar'   => true,
    'menu_position'       => 5,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'capability_type'     => 'page',
    'menu_icon'           => 'dashicons-clipboard',
  );

  register_post_type( 'guru_case_study', $args );

}
add_action( 'init', 'custom_post_type_guru_case_study', 0 );

// Register Custom Post Type
function saving_hotels_hotels() {

    $labels = array(
        'name'                  => 'Hotels',
        'singular_name'         => 'Hotel',
        'menu_name'             => 'Hotels',
        'name_admin_bar'        => 'Hotels',
        'archives'              => 'Item Archives',
        'attributes'            => 'Item Attributes',
        'parent_item_colon'     => 'Parent Item:',
        'all_items'             => 'All Items',
        'add_new_item'          => 'Add New Item',
        'add_new'               => 'Add New',
        'new_item'              => 'New Item',
        'edit_item'             => 'Edit Item',
        'update_item'           => 'Update Item',
        'view_item'             => 'View Item',
        'view_items'            => 'View Items',
        'search_items'          => 'Search Item',
        'not_found'             => 'Not found',
        'not_found_in_trash'    => 'Not found in Trash',
        'featured_image'        => 'Featured Image',
        'set_featured_image'    => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image'    => 'Use as featured image',
        'insert_into_item'      => 'Insert into item',
        'uploaded_to_this_item' => 'Uploaded to this item',
        'items_list'            => 'Items list',
        'items_list_navigation' => 'Items list navigation',
        'filter_items_list'     => 'Filter items list',
    );
    $args = array(
        'label'                 => 'Hotel',
        'description'           => 'Description',
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail' ),
        'taxonomies'            => array( 'saving-tax' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-building',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => false,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'saving_hotels', $args );

}
add_action( 'init', 'saving_hotels_hotels', 0 );

// Register Custom Post Type
function guru_webinars_cpt() {

    $labels = array(
        'name'                  => 'Webinars',
        'singular_name'         => 'Webinar',
        'menu_name'             => 'Webinars',
        'name_admin_bar'        => 'Webinars',
        'archives'              => 'Item Archives',
        'attributes'            => 'Item Attributes',
        'parent_item_colon'     => 'Parent Item:',
        'all_items'             => 'All Items',
        'add_new_item'          => 'Add New Item',
        'add_new'               => 'Add New',
        'new_item'              => 'New Item',
        'edit_item'             => 'Edit Item',
        'update_item'           => 'Update Item',
        'view_item'             => 'View Item',
        'view_items'            => 'View Items',
        'search_items'          => 'Search Item',
        'not_found'             => 'Not found',
        'not_found_in_trash'    => 'Not found in Trash',
        'featured_image'        => 'Featured Image',
        'set_featured_image'    => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image'    => 'Use as featured image',
        'insert_into_item'      => 'Insert into item',
        'uploaded_to_this_item' => 'Uploaded to this item',
        'items_list'            => 'Items list',
        'items_list_navigation' => 'Items list navigation',
        'filter_items_list'     => 'Filter items list',
    );
    $args = array(
        'label'                 => 'Webinar',
        'description'           => 'Description',
        'labels'                => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-video',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => false,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'rewrite'             => array('slug' => 'webinar')
    );
    register_post_type( 'guru_webinar', $args );

}
add_action( 'init', 'guru_webinars_cpt', 0 );

// Register Custom Post Type
function guru_members_cpt() {

    $labels = array(
        'name'                  => 'Members',
        'singular_name'         => 'Member',
        'menu_name'             => 'Members',
        'name_admin_bar'        => 'Members',
        'archives'              => 'Item Archives',
        'attributes'            => 'Item Attributes',
        'parent_item_colon'     => 'Parent Item:',
        'all_items'             => 'All Items',
        'add_new_item'          => 'Add New Item',
        'add_new'               => 'Add New',
        'new_item'              => 'New Item',
        'edit_item'             => 'Edit Item',
        'update_item'           => 'Update Item',
        'view_item'             => 'View Item',
        'view_items'            => 'View Items',
        'search_items'          => 'Search Item',
        'not_found'             => 'Not found',
        'not_found_in_trash'    => 'Not found in Trash',
        'featured_image'        => 'Featured Image',
        'set_featured_image'    => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image'    => 'Use as featured image',
        'insert_into_item'      => 'Insert into item',
        'uploaded_to_this_item' => 'Uploaded to this item',
        'items_list'            => 'Items list',
        'items_list_navigation' => 'Items list navigation',
        'filter_items_list'     => 'Filter items list',
    );
    $args = array(
        'label'                 => 'Member',
        'description'           => 'Description',
        'labels'                => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-buddicons-buddypress-logo',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => false,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'guru_member', $args );

}
add_action( 'init', 'guru_members_cpt', 0 );

// Register Custom Post Type
function guru_press_cpt() {

    $labels = array(
        'name'                  => 'Press',
        'singular_name'         => 'Press',
        'menu_name'             => 'Press',
        'name_admin_bar'        => 'Press',
        'archives'              => 'Item Archives',
        'attributes'            => 'Item Attributes',
        'parent_item_colon'     => 'Parent Item:',
        'all_items'             => 'All Items',
        'add_new_item'          => 'Add New Item',
        'add_new'               => 'Add New',
        'new_item'              => 'New Item',
        'edit_item'             => 'Edit Item',
        'update_item'           => 'Update Item',
        'view_item'             => 'View Item',
        'view_items'            => 'View Items',
        'search_items'          => 'Search Item',
        'not_found'             => 'Not found',
        'not_found_in_trash'    => 'Not found in Trash',
        'featured_image'        => 'Featured Image',
        'set_featured_image'    => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image'    => 'Use as featured image',
        'insert_into_item'      => 'Insert into item',
        'uploaded_to_this_item' => 'Uploaded to this item',
        'items_list'            => 'Items list',
        'items_list_navigation' => 'Items list navigation',
        'filter_items_list'     => 'Filter items list',
    );
    $args = array(
        'label'                 => 'Press',
        'description'           => 'Description',
        'labels'                => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-admin-site-alt3',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => false,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'guru_press', $args );

}
add_action( 'init', 'guru_press_cpt', 0 );

// Register Custom Taxonomy
function saving_hotels_tax() {

    $labels = array(
        'name'                       => 'Ubicaciones',
        'singular_name'              => 'Ubicación',
        'menu_name'                  => 'Ubicaciones',
        'all_items'                  => 'All Items',
        'parent_item'                => 'Parent Item',
        'parent_item_colon'          => 'Parent Item:',
        'new_item_name'              => 'New Item Name',
        'add_new_item'               => 'Add New Item',
        'edit_item'                  => 'Edit Item',
        'update_item'                => 'Update Item',
        'view_item'                  => 'View Item',
        'separate_items_with_commas' => 'Separate items with commas',
        'add_or_remove_items'        => 'Add or remove items',
        'choose_from_most_used'      => 'Choose from the most used',
        'popular_items'              => 'Popular Items',
        'search_items'               => 'Search Items',
        'not_found'                  => 'Not Found',
        'no_terms'                   => 'No items',
        'items_list'                 => 'Items list',
        'items_list_navigation'      => 'Items list navigation',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
    );
    register_taxonomy( 'saving-tax', array( 'saving_hotels' ), $args );

}
add_action( 'init', 'saving_hotels_tax', 0 );

// Register Custom Taxonomy
function guru_live_type() {

    $labels = array(
        'name'                       => 'Tipos de Contenido',
        'singular_name'              => 'Tipos de Contenido',
        'menu_name'                  => 'Tipos de Contenido',
        'all_items'                  => 'All Items',
        'parent_item'                => 'Parent Item',
        'parent_item_colon'          => 'Parent Item:',
        'new_item_name'              => 'New Item Name',
        'add_new_item'               => 'Add New Item',
        'edit_item'                  => 'Edit Item',
        'update_item'                => 'Update Item',
        'view_item'                  => 'View Item',
        'separate_items_with_commas' => 'Separate items with commas',
        'add_or_remove_items'        => 'Add or remove items',
        'choose_from_most_used'      => 'Choose from the most used',
        'popular_items'              => 'Popular Items',
        'search_items'               => 'Search Items',
        'not_found'                  => 'Not Found',
        'no_terms'                   => 'No items',
        'items_list'                 => 'Items list',
        'items_list_navigation'      => 'Items list navigation',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
        'show_in_rest'               => true
    );
    register_taxonomy( 'guru_live_type', array( 'guru_live' ), $args );

}
add_action( 'init', 'guru_live_type', 0 );

// Register Custom Taxonomy
function guru_job_offer_type() {

    $labels = array(
        'name'                       => 'Área',
        'singular_name'              => 'Área',
        'menu_name'                  => 'Área',
        'all_items'                  => 'All Items',
        'parent_item'                => 'Parent Item',
        'parent_item_colon'          => 'Parent Item:',
        'new_item_name'              => 'New Item Name',
        'add_new_item'               => 'Add New Item',
        'edit_item'                  => 'Edit Item',
        'update_item'                => 'Update Item',
        'view_item'                  => 'View Item',
        'separate_items_with_commas' => 'Separate items with commas',
        'add_or_remove_items'        => 'Add or remove items',
        'choose_from_most_used'      => 'Choose from the most used',
        'popular_items'              => 'Popular Items',
        'search_items'               => 'Search Items',
        'not_found'                  => 'Not Found',
        'no_terms'                   => 'No items',
        'items_list'                 => 'Items list',
        'items_list_navigation'      => 'Items list navigation',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
        'show_in_rest'               => true
    );
    register_taxonomy( 'guru_job_offer_type', array( 'guru_job' ), $args );

}
add_action( 'init', 'guru_job_offer_type', 0 );

// Register Custom Post Type
function guru_advisory_service_cpt() {

    $labels = array(
        'name'                  => 'Advisory Services',
        'singular_name'         => 'Advisory Service',
        'menu_name'             => 'Advisory Services',
        'name_admin_bar'        => 'Advisory Services',
        'archives'              => 'Item Archives',
        'attributes'            => 'Item Attributes',
        'parent_item_colon'     => 'Parent Item:',
        'all_items'             => 'All Items',
        'add_new_item'          => 'Add New Item',
        'add_new'               => 'Add New',
        'new_item'              => 'New Item',
        'edit_item'             => 'Edit Item',
        'update_item'           => 'Update Item',
        'view_item'             => 'View Item',
        'view_items'            => 'View Items',
        'search_items'          => 'Search Item',
        'not_found'             => 'Not found',
        'not_found_in_trash'    => 'Not found in Trash',
        'featured_image'        => 'Featured Image',
        'set_featured_image'    => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image'    => 'Use as featured image',
        'insert_into_item'      => 'Insert into item',
        'uploaded_to_this_item' => 'Uploaded to this item',
        'items_list'            => 'Items list',
        'items_list_navigation' => 'Items list navigation',
        'filter_items_list'     => 'Filter items list',
    );
    $args = array(
        'label'                 => 'advisory-service',
        'description'           => 'Description',
        'labels'                => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_icon'             => 'dashicons-hammer',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => false,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'guru_service', $args );

}
add_action( 'init', 'guru_advisory_service_cpt', 0 );

// Register Custom Post Type
function guru_live_cpt() {

    $labels = array(
        'name'                  => 'Lives',
        'singular_name'         => 'Live',
    );
    $args = array(
        'label'                 => 'live',
        'description'           => 'Description',
        'labels'                => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_icon'             => 'dashicons-video-alt3',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => false,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
        'rewrite' => array('slug' => 'lives'),
    );
    register_post_type( 'guru_live', $args );

}
add_action( 'init', 'guru_live_cpt', 0 );

// Register Custom Post Type
function guru_job_cpt() {

    $labels = array(
        'name'                  => 'Job Offers',
        'singular_name'         => 'Job Offer',
    );
    $args = array(
        'label'                 => 'job-offer',
        'description'           => 'Description',
        'labels'                => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_icon'             => 'dashicons-clipboard',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => false,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
        'rewrite' => array('slug' => 'job-offer'),
    );
    register_post_type( 'guru_job', $args );

}
add_action( 'init', 'guru_job_cpt', 0 );

//Blocks

//Intro

add_action('acf/init', 'guru_init_block_types');
function guru_init_block_types() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        acf_register_block_type(array(
            'name'              => 'intro',
            'title'             => __('Guru Section - Intro'),
            'description'       => __('A custom intro block.'),
            'render_template'   => 'blocks/intro.php',
            'category'          => 'formatting',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'intro', 'introduction', 'introduccion' ),
        ));

        acf_register_block_type(array(
            'name'              => 'section-columns',
            'title'             => __('Guru Section - Columns'),
            'description'       => __('A custom section-columns block.'),
            'render_template'   => 'blocks/section-columns.php',
            'category'          => 'formatting',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'section-columns', 'columnas' ),
        ));

        acf_register_block_type(array(
            'name'              => 'job-offers',
            'title'             => __('Guru Section - Job Offers'),
            'description'       => __('A custom Job Offers block.'),
            'render_template'   => 'blocks/job-offers.php',
            'category'          => 'formatting',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'job-offers', 'careers', 'ofertas', 'trabajo' ),
        ));

        acf_register_block_type(array(
            'name'              => 'steps',
            'title'             => __('Guru Section - Steps'),
            'description'       => __('A custom Steps block.'),
            'render_template'   => 'blocks/steps.php',
            'category'          => 'formatting',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'steps', 'pasos' ),
        ));

        acf_register_block_type(array(
            'name'              => 'cta',
            'title'             => __('Guru Section - CTA'),
            'description'       => __('A custom CTA block.'),
            'render_template'   => 'blocks/cta.php',
            'category'          => 'formatting',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'cta', 'call to action' ),
        ));

        acf_register_block_type(array(
            'name'              => 'videos',
            'title'             => __('Guru Section - Videos'),
            'description'       => __('A custom videos block.'),
            'render_template'   => 'blocks/videos.php',
            'category'          => 'formatting',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'videos', 'video', 'embed' ),
        ));
    }
}
