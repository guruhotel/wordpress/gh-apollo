//Imports
import slick from 'slick-carousel';
import AOS from 'aos';
import lity from 'lity';
import Tabby from 'tabbyjs';
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

//Site code
jQuery(document).ready(function($){

    //Animations
    AOS.init();

    //Sliders
    $('.page-template-home .quotes-slider, .page-template-pricing .quotes-slider').slick({
        asNavFor: '.quotes-logos',
    });

    $('.page-template-home .quotes-logos, .page-template-pricing .quotes-logos').slick({
    asNavFor: '.quotes-slider',
    slidesToShow: 6,
    arrows: 0,
    responsive: [
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 4,
          infinite: true,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
            }
          }
        ]
    });

    $('.page-template-optimized-home .quotes-slider, page-template-pricing .quotes-slider').slick({
        slidesToShow: 3,
        arrows: 0,
        autoplay: true,
        speed: 1000,
        autoplaySpeed: 5000,
        pauseOnHover: false,
        responsive: [
          {
            breakpoint: 1025,
            settings: {
              slidesToShow: 2,
              infinite: true,
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              infinite: true,
            }
          }
        ]
    });

    $('.page-template-optimized-home .steps__illustrations, .block__steps .steps__illustrations').slick({
        arrows: 0,
        autoplay: true,
        fade: true,
        speed: 800,
        pauseOnHover: false
    });

    $('.page-template-optimized-home .banner-features__slider').slick({
        slidesToShow: 3,
        arrows: 0,
        autoplay: true,
        adaptiveHeight: true,
        autoplaySpeed: 10000,
        focusOnSelect: true,
        responsive: [
          {
            breakpoint: 1000,
            settings: {
              slidesToShow: 3,
              infinite: true,
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              infinite: true,
            }
          }
        ]
    })

    $('.page-template-optimized-home .banner-features__slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $('.page-template-optimized-home .banner-features__slider video').each(function(){
            $(this)[0].pause();
        });
    });

    $('.page-template-optimized-home .banner-features__slider').on('afterChange', function(event, slick, currentSlide){
        var video = $('.page-template-optimized-home .banner-features__slider .slick-slide.slick-current video');
        video[0].play();
    });

    $('.page-template-optimized-home .steps__illustrations, .block__steps .steps__illustrations').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $('.steps__step--active').removeClass('steps__step--active');
        $('.steps__step').eq(nextSlide).addClass('steps__step--active');
    });

    $('.steps__step').click(function(e) {
        e.preventDefault();
        var slideno = $(this).data('target');
        $('.steps__step--active').removeClass('steps__step--active');
        $(this).addClass('steps__step--active');
        $('.page-template-optimized-home .steps__illustrations').slick('slickGoTo', slideno);
    });

    $('.page-template-live .section-intro, .page-template-webinars .section-intro').slick({
        arrows: false,
        rows: 0,
        dots: true,
        adaptiveHeight: true
    });

    $('.members__slider').slick({
        arrows: false,
        rows: 0,
        adaptiveHeight: true,
        asNavFor: '.members__img-slider',
        fade: true,
    });

    $('.members__img-slider').slick({
        arrows: false,
        rows: 0,
        adaptiveHeight: true,
        asNavFor: '.members__slider',
        slidesToShow: 3,
        responsive: [
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 4,
              infinite: true,
            }
          }
        ]
    });

    $('.members__img-slider .slick-slide').click(function(e) {
       e.preventDefault();
       var slideno = $(this).data('slick-index');
       $('.members__slider').slick('slickGoTo', slideno);
     });

    $('.about-team__members').slick({
        slidesToShow: 4,
        responsive: [
          {
            breakpoint: 1000,
            settings: {
              slidesToShow: 3,
              infinite: true,
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
            }
          }
        ]
    });


    $('.page-template-home .quotes-logos a, .page-template-pricing .quotes-logos a').click(function(event){
        event.preventDefault();
        var slickIndex = $(this).parents('.slick-slide').data('slick-index');
        console.log(slickIndex);
        $('.quotes-slider').slick('slickGoTo', slickIndex);
    });

    //Nav tabs

    $('.nav-tabs a').click(function(event){
        event.preventDefault();
        var target = $(this).attr('href');
        var modal = $(this).parents('.modal-hotel');

        $(modal).find('.nav-tabs a.current').removeClass('current');
        $(this).addClass('current');

        $(modal).find('.tab-content__wrapper .tab.tab--active').removeClass('tab--active').hide();
        $(modal).find(target).addClass('tab--active').fadeIn();

        if(target == '#tab--gallery'){
            $(modal).find(target).find('.hotel-modal__gallery-slider').slick({
                rows: 0
            })
        }
    });

    //Hotels

    $('#hotels-location__select').on('change', function () {
          var url = $(this).val();
          if (url) {
            window.location = url;
          }
          return false;
      });

    $('[data-toggle="modal"], .modal-hotel button.close').click(function(event){
        event.preventDefault();
        var target = $(this).attr('data-target');

        if($(target).hasClass('show') && $(target).find('.hotel-modal__gallery-slider').hasClass('slick-initialized')){
            $(target).find('.hotel-modal__gallery-slider').slick('unslick');
            $(target).find('.nav-tabs a.current').removeClass('current');
            $(target).find('.tab-content__wrapper .tab.tab--active').removeClass('tab--active').hide();

            $(target).find('.nav-tabs li:first-of-type a').addClass('current');
            $(target).find('.tab-content__wrapper .tab:first-of-type').addClass('tab--active').show();
        }

        if($(target).hasClass('show')){
            $(target).fadeOut();
        }
        else{
            $(target).fadeIn();
        }

        $(target).toggleClass('show');
    });

    $('.modal-hotel .modal-fade').click(function(event){
        event.preventDefault();
        var target = $(this).parent('.modal-hotel');

        if($(target).hasClass('show') && $(target).find('.hotel-modal__gallery-slider').hasClass('slick-initialized')){
            $(target).find('.hotel-modal__gallery-slider').slick('unslick');
            $(target).find('.nav-tabs a.current').removeClass('current');
            $(modal).find('.tab-content__wrapper .tab.tab--active').removeClass('tab--active').hide();

            $(target).find('.nav-tabs li:first-of-type a').addClass('current');
            $(modal).find('.tab-content__wrapper .tab:first-of-type').addClass('tab--active');
        }

        if($(target).hasClass('show')){
            $(target).fadeOut();
        }
        else{
            $(target).fadeIn();
        }

        $(target).toggleClass('show');
    });

    //Tabs and order

    $('.webinars__order-by-btn').click(function(event){
        event.preventDefault();
        var target = $(this).attr('href');

        $('.webinars__order-by li.current').removeClass('current');
        $('.webinars__order-by-btn[href="'+target+'"]').parents('li').addClass('current');

        $('.webinar-list__webinars.active').removeClass('active').fadeOut();
        $(target).fadeIn().addClass('active');
    });

    $('.jobs__categories a').click(function(event){
        event.preventDefault();
        var target = '.'+$(this).data('cat');

        $('.jobs__categories a.category--active').removeClass('category--active');
        $(this).addClass('category--active');

        $('.jobs__listing article').removeClass('job__post--active').fadeOut();
        $(target).fadeIn().addClass('job__post--active');
    });

    $('.live__order-by-btn').click(function(event){
        event.preventDefault();
        var target = $(this).attr('href');

        $('.live__order-by li.current').removeClass('current');
        $('.live__order-by-btn[href="'+target+'"]').parents('li').addClass('current');

        $('.live-item.active').removeClass('active').fadeOut();
        $(target).fadeIn().addClass('active');
    });

    //Actions

    $('.corporative-image-faq__list button, .corporative-image-faq__list header h4').click(function(event){
        event.preventDefault();
        $(this).parents('.item').find('.answer').slideToggle();
    });

    $('.corporative-image-forms .form-selector li').click(function(event){
        event.preventDefault();
        var target = $(this).data('form');
        console.log(target);

        $('.corporative-image-forms .form-selector li.active').removeClass('active');
        $(this).addClass('active');


        $('.corporative-image-forms__form.active').fadeOut().removeClass('active');
        $('.corporative-image-forms__form[data-form="'+target+'"').fadeIn().addClass('active');
    });

    //Scroll animations
    $.fn.isInViewport = function(offsetTop) {
      var elementTop = $(this).offset().top + offsetTop;
      var elementBottom = elementTop + $(this).outerHeight();
      var viewportTop = $(window).scrollTop();
      var viewportBottom = viewportTop + $(window).height();
      return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    $.fn.topViewport = function(offsetTop) {
      var elementTop = $(this).offset().top + offsetTop;
      var viewportTop = $(window).scrollTop();
      return elementTop < viewportTop;
    };

    $(window).on('resize scroll', function() {

        // STEPS

        $('.page-template-home #home-steps .steps__step').each(function() {
          if ($(this).topViewport(-200)) {
            var target = $(this).data('target');
           $('#home-steps .steps__step.steps__step--active').removeClass('steps__step--active');
           $(this).addClass('steps__step--active');
           $('.steps__illustration--active').removeClass('steps__illustration--active');
           $('.steps__illustration--active').fadeOut;
           $(target).addClass('steps__illustration--active');
          }
        });

        $('.page-template-home #home-steps .steps__step:first-of-type').each(function() {
           if ($(this).topViewport(-100)) {
                $(this).parents('#home-steps').addClass('home-steps--active');
           }
           else{
            $(this).parents('#home-steps').removeClass('home-steps--active');
           }
        });

        $('.page-template-home #home-steps').each(function() {
           if (!$(this).isInViewport(-100)) {
                $(this).removeClass('home-steps--active');
           }
        });

        // MARKETPLACE
        $('#home-marketplace').each(function() {
          if ($(this).topViewport(0)) {
            $(this).addClass('home-marketplace--active');
          }
          else{
            $(this).removeClass('home-marketplace--active');
          }

          if(!$(this).isInViewport(0)){
            $(this).removeClass('home-marketplace--active');
          }
        });

        $('.page-template-home #home-quotes').each(function() {
          if ($(this).isInViewport(-120)) {
            $('#home-marketplace').removeClass('home-marketplace--active');
          }
        });

        //Animate lines
        $('.corporative-image-services').each(function() {
          if ($(this).topViewport(-600)) {
            $(this).addClass('corporative-image-services--active');
          }
          else{
            $(this).removeClass('corporative-image-services--active');
          }
        });
    });


    //SavingIndieHotels

    if($('body').hasClass('page-template-saving')){
        var tabs = new Tabby('[data-tabs]');

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };

        // Filter by query URL
        var query = getUrlParameter('filter');
        if(query) {
            tabs.toggle(`#tab-${query}`);
        }

        $('#saving .selector ul li.option').click(function() {
            var a = $(this).siblings().toggle();
            $(this).parent().prepend(this);
        })
    }

    //New home
    var logosCount = 0;
    if($('body').hasClass('page-template-optimized-home')){
        var totalCount = 0;

        if($(window).width() > 768){
            totalCount = 6;
        }
        else{
            totalCount = 3;

            $('.page-template-optimized-home .banner-integrations__logos .logo-animated:nth-child(n+4)').addClass('logo-initial-hidden');
        }

        setInterval(function(){

            if(logosCount < totalCount){
                $('.banner-integrations__logos .logo-animated').eq(logosCount).addClass('logo-hidden');
                $('.banner-integrations__logos .logo-animated').eq(logosCount+totalCount).addClass('logo-active');
            }

            logosCount++;

            if(logosCount > (totalCount-1)){
                $('.banner-integrations__logos .logo-hidden').addClass('logo-initial-hidden').removeClass('logo-hidden');
                $('.banner-integrations__logos .logo-animated').eq(logosCount-totalCount).removeClass('logo-initial-hidden');
                $('.banner-integrations__logos .logo-animated').eq(logosCount).removeClass('logo-active').removeClass('logo-initial-hidden').addClass('logo-hidden-second');
            }

            if(logosCount == (totalCount*2 - 1)){
                logosCount = 0;
            }
        }, 3000);

        setInterval(function(){

            if(logosCount < totalCount){
                $('.quotes-logos .logo-animated').eq(logosCount).addClass('logo-hidden');
                $('.quotes-logos .logo-animated').eq(logosCount+totalCount).addClass('logo-active');
            }

            logosCount++;

            if(logosCount > (totalCount-1)){
                $('.quotes-logos .logo-hidden').addClass('logo-initial-hidden').removeClass('logo-hidden');
                $('.quotes-logos .logo-animated').eq(logosCount-totalCount).removeClass('logo-initial-hidden');
                $('.quotes-logos .logo-animated').eq(logosCount).removeClass('logo-active').removeClass('logo-initial-hidden').addClass('logo-hidden-second');
            }

            if(logosCount == (totalCount*2 - 1)){
                logosCount = 0;
            }
        }, 3000);
    }

    //Website
    if($('body').hasClass('page-template-website') && $(window).width() > 768){

        var bodyGap = ($('body').width() - $('.website-sections>.container-fluid').width()) / 2 - 16; //32 is the padding of the container
        var colWidth = $('.website-sections .col-md-6').width() + 35;

        var rotate = gsap.timeline({
          scrollTrigger:{
            trigger: "#website-intro__animation",
            pin: false,
            scrub:0.2,
            start: 'top top',
            end:'+400',
            trigger: 'body',
          }
        })
        .to('#website-intro__animation', {
          rotation:0,
          top: 700,
          width: colWidth,
          opacity: 0,
          right: bodyGap,
          duration:1, ease:'none',
        })

        var firstSectionAppear = gsap.timeline({
          scrollTrigger:{
            trigger: "#first-section",
            pin: false,
            scrub:0.2,
            start: '200',
            end:'+350',
            trigger: 'body',
          }
        })
        .to('#first-section', {
          opacity: 1,
          duration:.5, ease:'none',
        })

        var downsize = gsap.timeline({
          scrollTrigger:{
            trigger: "#website-intro__circle",
            pin: false,
            scrub:0.2,
            start: 'top top',
            end:'+400',
            trigger: 'body',
          }
        })
        .to('#website-intro__circle', {
          rotation:0,
          width: 2000,
          height: 2000,
          opacity: 0,
          duration:1, ease:'none',
        })
    }


    //Pricing
    function calculatePricing(){
        $('.pricing-calculator__comissions .item').each(function(){
                var value = parseFloat($('#pricing-calculator-user-input').val());

                if(value < 10){
                    $('.pricing-calculator .error').addClass('active');
                }
                else{
                    $('.pricing-calculator .error').removeClass('active');
                    var percentage = parseFloat($(this).find('.pricing-result').data('percentage'));
                    var fixed = parseFloat($(this).find('.pricing-result').data('fixed'));
                    var comission = (percentage * value / 100) + fixed;
                    console.log(comission);
                    var result = value - comission;

                    $(this).find('.pricing-result').text('$'+result.toFixed(2));
                }
            });
        }
        calculatePricing();

    $('#pricing-calculator-user-input').keyup(function(){
        calculatePricing();
    });

    if($('body').hasClass('page-template-corporative-image')){
        let baslideActive = false;
        document.querySelector('.scroller').addEventListener('mousedown',function(){
          baslideActive = true;
          document.querySelector('.scroller').classList.add('scrolling');
        });
        document.body.addEventListener('mouseup',function(){
          baslideActive = false;
          document.querySelector('.scroller').classList.remove('scrolling');
        });
        document.body.addEventListener('mouseleave',function(){
          baslideActive = false;
          document.querySelector('.scroller').classList.remove('scrolling');
        });

        // Let's figure out where their mouse is at
        document.body.addEventListener('mousemove',function(e){
          if (!baslideActive) return;
          let x = e.pageX;
          x -= document.querySelector('.ba-slider-wrapper').getBoundingClientRect().left;
          scrollIt(x);
        });

        // Let's use this function
        function scrollIt(x){
            let transform = Math.max(0,(Math.min(x,document.querySelector('.ba-slider-wrapper').offsetWidth)));
            document.querySelector('.after').style.width = transform+"px";
            document.querySelector('.scroller').style.left = transform-10+"px";
        }

        scrollIt(150);

        document.querySelector('.scroller').addEventListener('touchstart',function(){
          baslideActive = true;
          document.querySelector('.scroller').classList.add('scrolling');
        });
        document.body.addEventListener('touchend',function(){
          baslideActive = false;
          document.querySelector('.scroller').classList.remove('scrolling');
        });
        document.body.addEventListener('touchcancel',function(){
          baslideActive = false;
          document.querySelector('.scroller').classList.remove('scrolling');
        });
    }

});

