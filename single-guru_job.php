<?php
get_header();
while(have_posts()): the_post();
?>
    <div class="job-offer__header background-color__titles text-color__white">
        <div class="container-fluid wrap">
            <div class="row middle-xs center-xs">
                <div class="col-xs-12 col-sm-8 start-xs">
                    <a href="<?php the_permalink( 2618 ); ?>" class="btn--text text-color__orange"><i class="fas fa-long-arrow-alt-left margin-right__medium text-color__orange"></i><?php _e('Back to ', 'gh-apollo'); echo get_the_title( 2618 ); ?></a>
                    <h2 class="text-color__white"><?php the_title(); ?></h2>
                    <span class="text-transform__uppercase letter-spacing__big font-family__secondary"><?php the_field('job_availability'); ?> | <?php the_field('job_expertise'); ?></span>
                </div>
                <div class="col-xs-12 col-sm-4 end-xs article-share start-xs">
                    <h5 class="text-color__white font-weight__semibold font-size__normal"><?php _e('Share this job', 'gh-apollo'); ?></h5>
                    <div class="margin-top__normal">
                        <a href="https://www.facebook.com/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-right__big text-color__white">
                            <i class="fab fa-facebook-f"></i>
                        </a>

                        <a href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-right__big text-color__white">
                            <i class="fab fa-twitter"></i>
                        </a>

                        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_the_permalink(); ?>" target="_blank" class="btn text-color__white">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="job-offer__content">
        <div class="container-fluid wrap">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-6">
                    <?php the_content(); ?>
                    <div class="article-share">
                        <hr>
                        <h5 class="text-color__text font-weight__semibold font-size__normal"><?php _e('Share this job', 'gh-apollo'); ?></h5>
                        <div class="margin-top__normal">
                            <a href="https://www.facebook.com/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank" class="text-color__titles">
                                <i class="fab fa-facebook-f"></i>
                            </a>

                            <a href="https://twitter.com/intent/tweet?url=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-left__big text-color__titles">
                                <i class="fab fa-twitter"></i>
                            </a>

                            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_the_permalink(); ?>" target="_blank" class="btn margin-left__big btn text-color__titles">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5 col-md-offset-1 column--form start-xs">
                    <h3><?php _e('Submit your application', 'gh-apollo'); ?></h3>
                    <?php echo do_shortcode( '[wpforms id="2633"]', false ); ?>
                </div>
            </div>
        </div>
    </div>

<?php endwhile; get_footer();
