<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <script type='text/javascript' src='//kit.fontawesome.com/549f1fa78b.js?ver=5.4.2'></script>
	<?php wp_head(); ?>
    <script data-ad-client="ca-pub-6166140885510145" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body <?php body_class(); ?>>
    <?php if(!is_page_template('page-templates/saving.php') & !is_page_template('page-templates/savingbuy.php') & !is_page_template('page-templates/login.php')) { ?>
        <!-- Start of Async Drift Code -->
        <script>
            "use strict";
            !function() {
            var t = window.driftt = window.drift = window.driftt || [];
            if (!t.init) {
                if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
                t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
                t.factory = function(e) {
                return function() {
                    var n = Array.prototype.slice.call(arguments);
                    return n.unshift(e), t.push(n), t;
                };
                }, t.methods.forEach(function(e) {
                t[e] = t.factory(e);
                }), t.load = function(t) {
                var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
                var i = document.getElementsByTagName("script")[0];
                i.parentNode.insertBefore(o, i);
                };
            }
            }();
            drift.SNIPPET_VERSION = '0.3.1';
            if(window.location.href.indexOf("/en") >  -1) {
                drift.config({
                 locale: "en",
                    messages:{
                        welcomeMessage: "Hey there, Welcome to the future!",
                        awayMessage: "We are taking a break but fire away!"
            }
            });
            }
           drift.load('nyt9ky6ibzcu');
        </script>
        <!-- End of Async Drift Code -->
    <?php } ?>

    <script>
      !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t,e){var n=document.createElement("script");n.type="text/javascript";n.async=!0;n.src="https://cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(n,a);analytics._loadOptions=e};analytics.SNIPPET_VERSION="4.1.0";
      analytics.load("EUy4M7LArfusmvgcYeGdxZ3eZIIs8zsp");
      analytics.page();
      }}();
    </script>


    <?php if (!get_field('hide_header')): ?>
        <header id="header" class="headbar <?php if (get_field('show_white_header')) echo 'header__white'; ?>">
            <div class="container-fluid wrap">
                <div class="row between-xs middle-xs">
                    <div class="col-xs-12 col-md-8">
                        <div class="brand">
                            <?php if (get_field('show_white_header')) : ?>
                                <a href="<?php bloginfo('wpurl'); ?>">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-inverse.svg">
                                </a>
                            <?php else: ?>
                                <a href="<?php bloginfo('wpurl'); ?>">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-original.svg">
                                </a>
                            <?php endif; ?>
                        </div>

                        <?php if (!get_field('hide_menu')) : ?>
                            <div class="menu">
                                <nav class="main-menu">
                                    <?php wp_nav_menu( array( 'theme_location' => 'menu-main', 'container' => '' ) ); ?>
                                </nav>
                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-4 end-xs">
                        <?php if (is_page_template('page-templates/saving.php') || is_page_template('page-templates/savingbuy.php')) : ?>
                            <a href="<?php the_field('header_redeem_btn_url', 'option'); ?>" class="btn btn--primary border-radius__normal background-color__main text-color__white padding__medium--x display__inline--block font-size__small--x" target="_blank"><?php _e('Redimir', 'apollo'); ?></a>
                        <?php elseif(!is_page_template('page-templates/landing-healthcare.php')): ?>
                            <a href="<?php the_field('header_create_btn_url', 'option'); ?>" class="btn btn--primary border-radius__normal <?php if (get_field('show_white_header')) echo 'background-color__white text-color__main'; else echo 'background-color__main text-color__white'; ?> hidden__xs hidden__sm padding__medium--x display__inline--block font-size__small--x"><?php _e('Create website', 'apollo'); ?></a>
                        <?php endif; ?>
                        <a href="<?php the_field('header_login_btn_url', 'option'); ?>" class="margin-left__big--x font-weight__medium text-color__titles hidden__xs hidden__sm"><?php _e('Login', 'gh-apollo'); ?></a>
                    </div>
                </div>
            </div>
        </header>
    <?php endif ?>


